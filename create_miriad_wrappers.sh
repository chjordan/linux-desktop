#!/usr/bin/env bash

# If your system libraries are not the same as those shipped with MIRIAD, then
# you've got a problem:
# 1) You can't put MIRIAD's library path before everything else in the
#    LD_LIBRARY_PATH, because that would break the rest of your system, and
# 2) You can't put MIRIAD's library path after everything else in the
#    LD_LIBRARY_PATH, because that would not allow MIRIAD to function.
# So, you get this ridiculous script to make a small wrapper script for every
# MIRIAD executable, which correctly sets the PATH and LD_LIBRARY_PATH.
# Requires ruby.


for x in /opt/miriad/linux64/bin/*; do
    base=`basename $x`
    if [[ $base == "miriad" ]]; then
        echo "#!/usr/bin/env bash

PATH=/opt/miriad/linux64/bin:\$PATH LD_LIBRARY_PATH=/opt/miriad/linux64/lib:\$LD_LIBRARY_PATH exec $x" > $base
    else
        echo "#!/usr/bin/env ruby

args = []
ARGV.each do |a|
    p, s = a.split('=')
    if s
        args.push(\"#{p}='#{s}'\")
    else
        args.push(\"#{p}\")
    end
end
exec \"LD_LIBRARY_PATH=/opt/miriad/linux64/lib:\$LD_LIBRARY_PATH /opt/miriad/linux64/bin/$base #{args.join(' ')}\"" > $base
    fi

    chmod +x $base
done
