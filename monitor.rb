#!/usr/bin/env ruby

# This script is best run as root, to avoid the annoying sudo prompt.
# You could comment out the xorg.conf linking, assuming the proper xorg.conf is in place.
# The linking to a dualhead profile is necessary, because otherwise optirun/primusrun requires a monitor to be plugged in (even if you're not using it).

require "colorize"

$primary = "LVDS1"
$secondary = "VIRTUAL1"

def generate_modeline
    `cvt #{$x} #{$y}`.match(/Modeline (.*)/)[1]
end

def set_up
    # Copy the xorg.conf that allows multihead
    puts ["Linking:".blue, "/etc/bumblebee/xorg.conf.nvidia{.multihead,}".green].join(" ")
    system("sudo ln -sf /etc/bumblebee/xorg.conf.nvidia{.multihead,}")

    # Use cvt to generate a new desired modeline
    modeline = generate_modeline
    mode = `echo #{modeline}`.split.first
    # Create the new mode
    system("xrandr --newmode #{modeline}")
    # Add the mode to the $secondary device
    system("xrandr --addmode #{$secondary} #{mode}")
    # Activate bumblebee and nvidia external monitor and set options for it
    xrandr_output_command = "xrandr --output #{$primary} --auto --output #{$secondary} --mode #{mode} --#{$pos}-of #{$primary}"
    puts ["Running:".blue, xrandr_output_command.green].join(" ")
    system(xrandr_output_command)
end

def clean_up
    # Deactivate monitor after screenclone is killed
    puts ["Restoring:".blue, "/etc/bumblebee/xorg.conf.nvidia{.default,}".green].join(" ")
    system("xrandr --output #{$secondary} --off")
    # Restore the xorg.conf that provides normal bumblebee behaviour
    system("sudo ln -sf /etc/bumblebee/xorg.conf.nvidia{.default,}")
end


# Default values
if ARGV.empty?
    $x, $y, $pos = 1920, 1080, "right"
else
    $x, $y = ARGV[0], ARGV[1]
    abort(["Aborting:".red, "x-size specified, but not y-size.".yellow].join(" ")) if $y.nil?
    $pos = ARGV[2].nil? ? "right" : ARGV[2]
end

# Find any modes that have been declared for the $secondary device and remove them
$secondary_found = false
`xrandr`.split("\n").each do |l|
    if l.include?($secondary)
        $secondary_found = true
        next
    end
    if $secondary_found and l =~ /^\s+/
        system("xrandr --delmode #{$secondary} #{l.split(" ").first.gsub("\"", "\\\"")}")
    # All modes have been removed from the $secondary device
    elsif $secondary_found
        break
    end
end

set_up
begin
    # Run screenclone in optirun
    optirun_command = "optirun screenclone -s :0 -d :8 -x 1"
    puts ["Running:".blue, optirun_command.green].join(" ")
    puts `#{optirun_command}`
    clean_up
rescue Interrupt
    clean_up
end
