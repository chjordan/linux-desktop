# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# For emacs' tramp
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ '

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

zstyle ':vcs_info:*:prompt:*' actionformats "${PR_RESET} on ${FMT_BRANCH}${FMT_ACTION}" "${FMT_PATH}"
zstyle ':vcs_info:*:prompt:*' formats		"${PR_RESET} on ${FMT_BRANCH}"              "${FMT_PATH}"
zstyle ':vcs_info:*:prompt:*' nvcsformats	""                           "%~"

setopt NOTIFY CORRECT PROMPT_SUBST NO_BEEP
setopt EXTENDEDGLOB COMPLETE_IN_WORD
setopt APPEND_HISTORY HIST_IGNORE_ALL_DUPS HIST_IGNORE_SPACE HIST_REDUCE_BLANKS NO_HIST_BEEP
autoload -Uz compinit bashcompinit promptinit vcs_info colors zmv

compinit     # tab-completion
bashcompinit # tab-completion with bash files
promptinit   # advanced prompt support
colors

# https://github.com/eevee/rc/blob/master/.zshrc
# Force a reload of completion system if nothing matched; this fixes installing
# a program and then trying to tab-complete its name
_force_rehash() {
    (( CURRENT == 1 )) && rehash
    return 1    # Because we didn't really complete anything
}

zstyle ":compinstall" filename "~/.zshrc"
zstyle ":completion:*" menu select
# zstyle ':completion:*:default' list-colors ''
zstyle ':completion:*:*' list-colors ''
# zstyle ":completion:*:default" list-colors "=(#b) #([0-9]#)*=0=01;31"
# Completers to use: rehash, general completion, then various magic stuff and
# spell-checking.  Only allow two errors when correcting
zstyle ':completion:*' completer _force_rehash _complete _ignored _match _correct _approximate _prefix
zstyle ':completion:*' max-errors 2

# When looking for matches, first try exact matches, then case-insensiive, then
# partial word completion
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'r:|[._-]=** r:|=**'

# Turn on caching, which helps with e.g. apt
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

# Show titles for completion types and group by type
zstyle ':completion:*:descriptions' format "$fg_bold[black]» %d$reset_color"
zstyle ':completion:*' group-name ''

# Always do mid-word tab completion
setopt complete_in_word

# http://zshwiki.org/home/examples/compsys/hostnames
[ -f ~/.ssh/config ] && : ${(A)ssh_config_hosts:=${${${${(@M)${(f)"$(<~/.ssh/config)"}:#Host *}#Host }:#*\**}:#*\?*}}
zstyle ':completion:*:*:*' hosts $ssh_config_hosts

zstyle ':vcs_info:*' enable git
# check-for-changes can be really slow.
# you should disable it, if you work with large repositories
zstyle ':vcs_info:*:prompt:*' check-for-changes true
zstyle ':vcs_info:*:prompt:*' unstagedstr '*'
zstyle ':vcs_info:*:prompt:*' stagedstr '+'

REPORTTIME=5

########################################
## Prompt - http://kriener.org/articles/2009/06/04/zsh-prompt-magic
for COLOR in BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
    eval PR_$COLOR='%{$fg[${(L)COLOR}]%}'
    eval PR_BRIGHT_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
done
PR_RESET="%{${reset_color}%}"

# alter the prompt colour and symbol for root
if [[ `whoami` != "root" ]]; then
    local user_colour=$PR_GREEN
    local symbol="$ "
else
    local user_colour=$PR_RED
    local symbol="# "
fi

local user="${user_colour}%n${PR_RESET}"
local machine="@${PR_CYAN}%M${PR_RESET}"

# if you're ssh'd, root or in screen, display the machine
if [[ ! -z $SSH_CONNECTION || `whoami` == "root" || ! -z $STY ]]; then
    local user="${user}${machine}"
fi

local vcs='$vcs_info_msg_0_'

export PROMPT="
${user} in ${PR_MAGENTA}"'${PWD/#$HOME/~}'"${PR_RESET}${vcs} at ${PR_CYAN}%D %T${PR_RESET}
${user_colour}${symbol}${PR_RESET}"
export PROMPT2="${PR_GREEN}>${PR_RESET} "
export SPROMPT="Correct $fg[red]%R$reset_color to $fg[green]%r $reset_color? (Yes, No, Abort, Edit) "


# VCS
FMT_BRANCH="${PR_BRIGHT_YELLOW}%b%u%c${PR_RESET}" # e.g. master
FMT_ACTION="(${PR_CYAN}%a${PR_RESET}%)"           # e.g. (rebase-i)
FMT_PATH="%R${PR_MAGENTA}/%S"                     # e.g. ~/repo/subdir
zstyle ':vcs_info:*:prompt:*' actionformats "${PR_RESET} on ${FMT_BRANCH}${FMT_ACTION}" "${FMT_PATH}"
zstyle ':vcs_info:*:prompt:*' formats		"${PR_RESET} on ${FMT_BRANCH}"              "${FMT_PATH}"
zstyle ':vcs_info:*:prompt:*' nvcsformats	""                           "%~"


########################################
## Key bindings
# Press Ctrl-V <key> to test. e.g.
# bindkey '^[[3~' delete-char

bindkey -e
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

[[ -n "${key[Home]}"   ]] && bindkey "${key[Home]}"   beginning-of-line
[[ -n "${key[End]}"    ]] && bindkey "${key[End]}"    end-of-line
# [[ -n "${key[Delete]}" ]] && bindkey "${key[Delete]}" delete-char
bindkey "^[/" history-beginning-search-backward
bindkey "^[?" history-beginning-search-forward

# allows the delete key to work in st
# function zle-line-init () { echoti smkx }
# function zle-line-finish () { echoti rmkx }
# zle -N zle-line-init
# zle -N zle-line-finish

autoload edit-command-line
zle -N edit-command-line
bindkey '^x^e' edit-command-line


########################################
## Functions
source_if_exists() {
    [ -r $1 ] && source $1
}
typeset -U path
function most_useless_use_of_zsh {
    local lines columns colour a b p q i pnew
    ((columns=COLUMNS-1, lines=LINES-1, colour=0))
    for ((b=-1.5; b<=1.5; b+=3.0/lines)) do
        for ((a=-2.0; a<=1; a+=3.0/columns)) do
            for ((p=0.0, q=0.0, i=0; p*p+q*q < 4 && i < 32; i++)) do
                ((pnew=p*p-q*q+a, q=2*p*q+b, p=pnew))
            done
            ((colour=(i/4)%8))
            echo -n "\\e[4${colour}m "
        done
        echo
    done
}


########################################
## Aliases
# ssh aliases
alias -g vnc0="-L 1025:localhost:5900"
alias -g vnc1="-L 1025:localhost:5901"
alias -g vnc2="-L 1025:localhost:5902"
alias -g vnc3="-L 1025:localhost:5903"


########################################
## Environment
# precmd and chpwd
precmd () { vcs_info 'prompt' }
case $TERM in
    linux | eterm-color) # things that don't need nor want titles
        ;;
    *)
        chpwd () { print -Pn "\e]2;%~\a" }
        print -Pn "\e]2;%~\a"
        ;;
esac


########################################
## Completions
# source_if_exists /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Arch's extra/pkgfile hook
source_if_exists /usr/share/doc/pkgfile/command-not-found.zsh

# Arch's community/zsh-syntax-highlighting
source_if_exists /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

########################################
## Everything else...
source_if_exists ~/.profile
