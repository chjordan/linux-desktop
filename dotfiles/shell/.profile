########################################
## Functions
source_if_exists () {
    [ -r $1 ] && source $1
}
add_path () {
    if [[ -r $1 ]]; then
        if [[ -z $PATH ]]; then
            PATH=$1
        elif [[ $PATH != *$1* ]]; then
            if [[ $2 == "prepend" ]]; then
                PATH=$1:$PATH
            else
                PATH+=:$1
            fi
        fi
    fi
}
each_version () {
    # bash specific
    shopt -s nullglob 2>/dev/null
    # zsh specific
    setopt nullglob 2>/dev/null
    if stat --printf='' $1* 2>/dev/null; then
        for x in $1*; do
            temp_path=($x $temp_path)
        done
        for x in $temp_path; do
            add_path $x/$2
        done
    fi
    shopt -u nullglob 2>/dev/null
    unsetopt nullglob 2>/dev/null
}
library_add_path () {
    if [[ -r $1 ]]; then
        if [[ -z $LD_LIBRARY_PATH ]]; then
            LD_LIBRARY_PATH=$1
        elif [[ $LD_LIBRARY_PATH != *$1* ]]; then
            if [[ $2 == "prepend" ]]; then
                LD_LIBRARY_PATH=$1:$LD_LIBRARY_PATH
            else
                LD_LIBRARY_PATH+=:$1
            fi
        fi
    fi
}
python_add_path () {
    if [[ -r $1 ]]; then
        if [[ -z $PYTHONPATH ]]; then
            PYTHONPATH=$1
        elif [[ $PYTHONPATH != *$1* ]]; then
            if [[ $2 == "prepend" ]]; then
                PYTHONPATH=$1:$PYTHONPATH
            else
                PYTHONPATH+=:$1
            fi
        fi
    fi
}
man_add_path () {
    if [[ -r $1 ]]; then
        if [[ -z $MANPATH ]]; then
            MANPATH=$1
        elif [[ $MANPATH != *$1* ]]; then
            if [[ $2 == "prepend" ]]; then
                MANPATH=$1:$MAN_PATH
            else
                MANPATH+=:$1
            fi
        fi
    fi
}

extract () {
    if [ -r $1 ]; then
        case $1 in
            *.tar*)      tar xvf $1     ;;
            *.bz2)       bunzip2 $1     ;;
            *.rar)       unrar x $1     ;;
            *.gz)        gunzip -k $1   ;;
            *.tbz2)      tar xvjf $1    ;;
            *.tgz)       tar xvzf $1    ;;
            *.zip)       unzip $1       ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1        ;;
            *)           echo "Don't know how to extract '$1'!" ;;
        esac
    else echo "'$1' is not a valid file!"
    fi
}

unlock_keychain () {
    keychain `find ~/.ssh/*.pub | cut -d. -f1-2`
    [ -r ~/.keychain/$HOST-sh ] && source ~/.keychain/$HOST-sh
}
[ -r ~/.keychain/$HOST-sh ] && source ~/.keychain/$HOST-sh

# mark/jump - shamelessly stolen from someone on the internet
export MARKPATH=~/.marks
function j {
    cd -P $MARKPATH/$1 2>/dev/null || echo "No such mark: $1"
}
function mark {
    mkdir -p $MARKPATH; ln -s $(pwd) $MARKPATH/$1
}
function unmark {
    rm -i $MARKPATH/$1
}
function marks {
    ls -l $MARKPATH | sed 's/  / /g' | cut -d' ' -f9- | sed 's/ -/\t-/g' && echo
}
_completemarks() {
    local cur wordlist
    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}
    wordlist=$(find $MARKPATH -type l -printf "%f\n")
    COMPREPLY=( $(compgen -W "${wordlist[@]}" -- "$cur") )
    return 0
}
complete -F _completemarks j unmark

in-path () {
    hash $1 2>/dev/null || return 1
}
in-path wakeonlan && alias wol="wakeonlan"
wake-betelgeuse () {
    wol d8:cb:8a:a2:17:12
}

# A simple command to empty whatever is in swap.
un-swap () {
    sudo swapoff -a && sudo swapon -a
}



########################################
## Environment
# Custom ls colours
[ -r ~/.dir_colors ] && eval `dircolors ~/.dir_colors`

# Custom man colours
man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
        LESS_TERMCAP_md=$'\E[01;38;5;74m' \
        LESS_TERMCAP_me=$'\E[0m' \
        LESS_TERMCAP_se=$'\E[0m' \
        LESS_TERMCAP_so=$'\E[38;5;246m' \
        LESS_TERMCAP_ue=$'\E[0m' \
        LESS_TERMCAP_us=$'\E[04;38;5;146m' \
        man "$@"
}


########################################
## Paths
## Ensure defaults are not insane.
library_add_path /usr/local/lib
library_add_path /usr/lib
[ -z $MANPATH ] && MANPATH=":"

export PATH
export LD_LIBRARY_PATH
export PYTHONPATH
export MANPATH


########################################
## Aliases
alias ls="ls -F --color=auto"
alias ll="ls -lhF --color=auto"
alias la="ls -lahF --color=auto"
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias sudo="sudo "
alias zcp="zmv -C"

alias verynice="ionice -c3 nice -n 15"
alias gitk="gitk --all"


########################################
## Machine-specific options
source_if_exists ~/.profile.local

