;;; init.el --- Load all of my emacs elisp code. -*- lexical-binding: t -*-

;;; Commentary:
;; Heavily inspired by https://github.com/purcell/emacs.d

;; This file just references a bunch of other files containing elisp code, in
;; an effort to keep things tidy, but also to prevent auto-installing a bunch
;; of packages I don't want on machines that I don't use frequently.

;; N.B. "-*- lexical-binding: t -*-" on the first line is required for the GC
;; code below.

;;; Code:

(let ((minver "25"))
  (when (version< emacs-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))
(when (version< emacs-version "26")
(message "Your Emacs is old, and some functionality probably won't work. Upgrade!"))

;; The directory "lisp" contains other elisp files for loading packages.
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; Tweak garbage collection upon startup.
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;; Super general tweaks.
(setq make-backup-files nil)
(setq inhibit-startup-screen t)
(setq column-number-mode t)
(setq create-lockfiles nil)
; Indentation spacing should be 4, unless stated otherwise.
(setq tab-width 4)
; No tabs.
(setq-default indent-tabs-mode nil)
; Make M-q a little wider.
(setq-default fill-column 80)
; Don't ask if it's OK to follow a symlink; just do it (a warning is still displayed).
(setq vc-follow-symlinks nil)
; Confirm yes-or-no prompts with a single letter.
(fset 'yes-or-no-p 'y-or-n-p)

(setq split-height-threshold nil)
(setq browse-url-browser-function 'browse-url-xdg-open)
(setq history-delete-duplicates t)
(setq x-stretch-cursor t)
(put 'downcase-region 'disabled nil)

; https://emacs.stackexchange.com/questions/10983/remember-permission-to-execute-risky-local-variables
(advice-add 'risky-local-variable-p :override #'ignore)

;; Packages and further tweaks.
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives
      '(("gnu-elpa"     . "https://elpa.gnu.org/packages/")
        ("melpa"        . "https://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("org"          . "https://orgmode.org/elpa/"))
      package-archive-priorities
      '(("org"          . 4)
        ("melpa"        . 3)
        ("melpa-stable" . 2)
        ("gnu-elpa"     . 1)))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)

(use-package delight
  :ensure t
  :config (message "Loaded delight")
  )

;; (use-package straight
;;   :ensure t
;;   :config (message "Loaded straight")
;;   )

(require 'init-built-in)
(require 'init-org)
(require 'init-appearance)
(require 'init-navigation)
(require 'init-ivy)
(require 'init-keys-and-functions)
(require 'init-dired)
(require 'init-multiple-cursors)

(require 'programming-general)
(require 'programming-other)
; Load more packages only for my personal machines.
(when (member (system-name) '("betelgeuse" "vega" "orion" "aldebaran" "bellatrix"))
  (require 'programming-lsp)
  (require 'programming-c)
  (require 'programming-crystal)
  (require 'programming-go)
  (require 'programming-haskell)
  ;; (require 'programming-julia)
  (require 'programming-latex)
  (require 'programming-lua)
  (require 'programming-python)
  (require 'programming-ruby)
  (require 'programming-rust)
  )

;; Things that don't belong anywhere else.
; Emacs support library for PDF files.
; https://github.com/politza/pdf-tools
(use-package pdf-tools
  :ensure t
  :defer  t
  :init   (pdf-tools-install t t nil nil)
  :config (message "Loaded pdf-tools")
  )

; Code auditing mode for Emacs
; https://github.com/chrisdone/audit
(let ((filename "~/.emacs.d/extra/audit/audit.el"))
  (when (file-exists-p filename)
    (load filename)))

;; Open my to-do list by default.
(let ((filename "~/todo.org"))
  (when (file-exists-p filename)
    (setq initial-buffer-choice filename)))

(setq custom-file "~/.emacs.d/emacs-custom.el")
(when (file-exists-p custom-file)
    (load custom-file))
(provide 'init)
;;; init.el ends here
(put 'upcase-region 'disabled nil)
