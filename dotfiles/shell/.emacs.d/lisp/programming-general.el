;;; programming-general.el --- Packages applicable to all programming modes.

;;; Commentary:
;; Things like company, but also cosmetics.

;;; Code:
(message "Reading programming-general.el")

; Modular in-buffer completion framework for Emacs http://company-mode.github.io/
; https://github.com/company-mode/company-mode
(use-package company
  :ensure  t
  ;; :init    (global-company-mode t)
  :hook    (prog-mode . company-mode)
  ;; :init    (add-hook 'prog-mode-hook (lambda () (company-mode t)))
  :bind    (:map company-mode-map ("C-TAB" . company-complete)
                                  ("C-." . company-files))
  :delight company-mode
  :config  (message "Loaded company")
           ;; (global-unset-key (kbd "C-x TAB"))
           ;; (define-key company-mode-map (kbd "C-x TAB") nil)
          ;; (setq company--disabled-backends (quote (company-files)))
           ;; (setq company-backends
           ;;       (quote
           ;;        (company-bbdb company-nxml company-css company-eclim company-semantic company-clang company-xcode company-cmake company-capf
           ;;                      (company-dabbrev-code company-gtags company-etags company-keywords)
           ;;                      company-oddmuse company-dabbrev)))
           ;; (setq company-backends
           ;;       (quote
           ;;        (company-capf (company-dabbrev-code company-gtags company-etags company-keywords))))
           (setq company-backends nil)
  )

(use-package yasnippet
  :ensure t
  :config (message "Loaded yasnippet")
  )

;; Project management
; https://github.com/magit/magit
; It's Magit! A Git porcelain inside Emacs. https://magit.vc
(use-package magit
  :ensure t
  :after  ivy
  :bind   ("C-c m" . magit-status)
  :config (message "Loaded magit")
          (setq magit-diff-refine-hunk (quote all))
          (setq magit-completing-read-function 'ivy-completing-read)
  )

; http://github.com/alphapapa/magit-todos
; Show source files' TODOs (and FIXMEs, etc) in Magit status buffer
(use-package magit-todos
  :ensure t
  :after magit
  :config (message "Loaded magit-todos")
          (magit-todos-mode)
  )

; https://github.com/bbatsov/projectile
; Project Interaction Library for Emacs
(use-package projectile
  :ensure t
  :defer  t
  :init   (projectile-mode)
  :config (message "Loaded projectile")
          ;; (projectile-mode +1)
          (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
          (setq projectile-completion-system (quote ivy))
          ; https://emacs.stackexchange.com/questions/10465/turn-on-projectile-mode-only-for-files-in-actual-projects?rq=1
          (setq projectile-require-project-root t)
          (setq projectile-enable-caching t)
          ;; (setq projectile--mode-line
          ;;       '(:eval (if (projectile-project-p)
          ;;                   (format " Projectile[%s]"
          ;;                           (projectile-project-name))
          ;;                 "")))
          ;; (setq projectile-mode-line-function '(lambda () (if (projectile-project-p)
          ;;                                                     (projectile-default-mode-line)
          ;;                                                   (ding)
          ;;                                                   (format " %s" "")
          ;;                                                   )))
  )

(use-package flymake :disabled t)
;; ;; Just until flycheck plays nicely with LSP (or I work out what I'm doing wrong)
;; (defun my-flymake-show-next-error ()
;;   (interactive)
;;   (flymake-goto-next-error)
;;   ;; (flymake-display-err-menu-for-current-line)
;;   )
;; (defun my-flymake-show-prev-error ()
;;   (interactive)
;;   (flymake-goto-prev-error)
;;   ;; (flymake-display-err-menu-for-current-line)
;;   )
;; (use-package flymake
;;   :bind (:map global-map ("C-c ! n" . 'my-flymake-show-next-error)
;;                          ("C-c ! p" . 'my-flymake-show-prev-error))
;; )
(use-package flycheck
  :ensure t
  :init   (global-flycheck-mode t)
  ;; :hook   (prog-mode . flycheck-mode)
  :bind (:map flycheck-mode-map ("M-N" . 'flycheck-next-error)
                                ("M-P" . 'flycheck-previous-error))
  :config (message "Loaded flycheck")
          (setq flycheck-idle-change-delay 3)
          (setq flycheck-check-syntax-automatically (quote (save idle-change mode-enabled)))
          (setq flycheck-disabled-checkers (quote (ruby-rubylint)))
  )

;; Code formatting
; https://github.com/lassik/emacs-format-all-the-code
; Auto-format source code in many languages with one command
(use-package format-all
  :ensure t
  :bind    (:map global-map ("C-c s" . format-all-buffer))
  :config (message "Loaded format-all")
  )

;; Navigation
; https://github.com/mickeynp/smart-scan
; Quickly jumps between other symbols found at point in Emacs.
(use-package smartscan
  :ensure t
  :init   (global-smartscan-mode 1)
  :config (message "Loaded smartscan")
  )

; https://github.com/justbur/emacs-which-key
; which-key is a minor mode for Emacs that displays the key bindings following your currently entered incomplete command (a prefix) in a popup.
(use-package which-key
  :ensure t
  :init   (which-key-mode)
  )
(use-package smartparens
  :ensure t
  :init   (smartparens-global-mode t)
          (show-smartparens-global-mode t)
  :bind   (:map global-map ("C-M-f" . sp-forward-sexp)
                           ("C-M-b" . sp-backward-sexp))
  :config (message "Loaded smartparens")
          (require 'smartparens-config)
  )

;; Searching
; https://github.com/Wilfred/ag.el
; An Emacs frontend to The Silver Searcher
(use-package ag
  :ensure t
  :defer  t
  :config (message "Loaded ag")
  )
(use-package pcre2el
  :ensure  t
  :delight pcre-mode
  :init    (pcre-mode t)
  :bind    (:map global-map ("M-%" . pcre-query-replace-regexp))
  :config  (message "Loaded pcre2el")
  )

;; Cosmetic
(use-package rainbow-delimiters
  :ensure t
  :config (message "Loaded rainbow-delimiters")
          (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  )

;; Spelling
(use-package flyspell
  :ensure  t
  :delight flyspell-mode " FlyS"
  :init    (add-hook 'prog-mode-hook 'flyspell-prog-mode)
           (add-hook 'text-mode-hook 'flyspell-mode)
  :config  (message "Loaded flyspell")
           (setq flyspell-issue-message-flag nil)
           (define-key flyspell-mode-map (kbd "C-.") nil)
  )

(use-package flyspell-correct-ivy
  :ensure t
  :after   flyspell
  :bind    (:map flyspell-mode-map ("C-;" . flyspell-correct-previous-word-generic)
                                   ("C-c $" . flyspell-correct-word-generic))
  :config  (message "Loaded flyspell-correct-ivy")
           (setq ispell-dictionary "british")
           (setq ispell-highlight-face (quote flyspell-incorrect))
  )

;; Misc.
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
(add-hook 'prog-mode-hook (lambda () (setq show-trailing-whitespace t)))

(provide 'programming-general)
;;; programming-general.el ends here
