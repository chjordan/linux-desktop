;;; programming-haskell.el --- My emacs setup for haskell programming.

;;; Commentary:

;;; Code:
(message "Reading programming-haskell.el")

(defun stylish-haskell ()
  "Execute stylish-haskell on the buffer."
  (interactive)
  (when (eq major-mode 'haskell-mode)
    (shell-command-to-string (format "stylish-haskell -i %s" buffer-file-name))
    ;; (shell-command-on-region nil nil "stylish-haskell -i" buffer-file-name t)
    (revert-buffer :ignore-auto :noconfirm)))

(use-package haskell-mode
  :ensure t
  :defer  t
  :init   (add-hook 'haskell-mode-hook 'haskell-decl-scan-mode)
  :bind   (:map haskell-mode-map
                ("C-c h" . hoogle)
                ("C-c s" . haskell-mode-stylish-buffer)
                ("C-c S" . stylish-haskell)
                ;; ("C-c S" . (haskell-mode-buffer-apply-command "stylish-haskell" "-i"))
                )
  :config
  (message "Loaded haskell-mode")
  (setq haskell-mode-stylish-haskell-path "brittany")
  ;; (setq haskell-stylish-on-save t)
  (setq haskell-hoogle-url "https://www.stackage.org/lts/hoogle?q=%s")
  )

(use-package intero
  :ensure t
  :defer  t
  ;; :init   (with-eval-after-load 'haskell-mode
  ;; 	    (intero-global-mode))
  :init   (add-hook 'haskell-mode-hook 'intero-mode)
  :after  (haskell-mode flycheck)
  :config (message "Loaded intero")
          (intero-global-mode 1)
          (flycheck-add-next-checker 'intero '(warning . haskell-hlint))
  )

;; (let ((filename "~/Software/other/intero/elisp/intero.el"))
;;   (when (file-exists-p filename)
;;     (load filename)
;;     (with-eval-after-load 'haskell-mode (intero-global-mode))
;;     (message "Loaded intero")
;;     (intero-global-mode 1)
;;     (flycheck-add-next-checker 'intero '(warning . haskell-hlint))
;;     ))

;; (use-package hasklig-mode
;;   :ensure t
;;   :init   (add-hook 'haskell-mode-hook 'hasklig-mode)
;;   :config
;;   (message "Loaded hasklig-mode")
;;   )

(provide 'programming-haskell)
;;; programming-haskell.el ends here
