;;; init-multiple-cursors.el --- My customisations for multiple-cursors and friends.

;;; Commentary:
;; I don't use this too often.

;;; Code:
(message "Reading init-multiple-cursors.el")

; https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :ensure t
  :defer  t
  :config (message "Loaded multiple-cursors")
  )

; Add Multiple Cursors using Ace Jump
; https://github.com/mm--/ace-mc
(use-package ace-mc
  :ensure t
  :defer  t
  :config (message "Loaded ace-mc")
  )

(provide 'init-multiple-cursors)
;;; init-multiple-cursors.el ends here
