;;; programming-lua.el --- My emacs setup for lua programming.

;;; Commentary:
;; I still have not really used lua, so I don't know how well this works.

;;; Code:
(message "Reading programming-lua.el")

(use-package lua-mode
  :ensure t
  :defer  t
  )

(provide 'programming-lua)
;;; programming-lua.el ends here
