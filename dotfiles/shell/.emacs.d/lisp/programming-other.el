;;; programming-other.el --- Syntax highlighting in various languages..

;;; Commentary:
;; Things like markdown, dockerfiles and YAML.

;;; Code:
(message "Reading programming-other.el")

(use-package markdown-mode
  :ensure t
  :defer  t
  :config (message "Loaded markdown-mode")
  )
; flymd - Emacs on the fly markdown preview
; https://github.com/mola-T/flymd
(use-package flymd
  :ensure t
  :defer  t
  :config (message "Loaded flymd")
          (setq flymd-close-buffer-delete-temp-files t)
          (setq flymd-output-directory "/tmp")
  )
(use-package json-mode
  :ensure t
  :defer  t
  :config (message "Loaded json-mode")
  )
(use-package yaml-mode
  :ensure t
  :defer  t
  :config (message "Loaded yaml-mode")
  )
(use-package toml-mode
  :ensure t
  :defer  t
  :config (message "Loaded toml-mode")
  )
(use-package csv-mode
  :ensure t
  :defer  t
  :config (message "Loaded csv-mode")
  )
(use-package cmake-mode
  :ensure t
  :defer  t
  :config (message "Loaded cmake-mode")
  )
(use-package meson-mode
  :ensure t
  :defer  t
  :config (message "Loaded meson-mode")
  )
(use-package dockerfile-mode
  :ensure t
  :defer  t
  :config (message "Loaded dockerfile-mode")
  )

;; (use-package docker
;;   :ensure t
;;   :defer  t
;;   :bind ("C-c d" . docker)
;;   :config (message "Loaded docker")
;;   )

;; (use-package ein
;;   :ensure t
;;   :defer  t
;;   :bind   ("C-c C-S-c" . ein:worksheet-execute-all-cell)
;;   :config (message "Loaded ein")
;;           (setq ein:completion-backend 'ein:use-ac-jedi-backend)
;;           (setq ein:polymode t)
;;   )

;; This seems to be quite slow? I'm not really using nix so I'm not worried.
;; (use-package nix-mode
;;   :ensure t
;;   :defer  t
;;   :config (message "Loaded nix-mode")
;;   )

(provide 'programming-other)
;;; programming-other.el ends here
