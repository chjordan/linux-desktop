;;; programming-ruby.el --- My emacs setup for ruby programming.

;;; Commentary:

;;; Code:
(message "Reading programming-ruby.el")

; https://github.com/castwide/solargraph
; Run: gem i solargraph rubocop
(use-package ruby-mode
  :defer  t
  :hook   (ruby-mode . lsp)
  :config (message "Loaded ruby-mode")
          (setq ruby-indent-level 4)
          (add-hook 'ruby-mode-hook 'flycheck-mode)
          (flycheck-add-next-checker 'lsp-ui '(warning . ruby-rubocop))
  )

;; (use-package ruby-mode
;;   :defer  t
;;   :init   (add-hook 'ruby-mode-hook 'robe-mode)
;;           (add-hook 'ruby-mode-hook 'inf-ruby-minor-mode)
;;   :config (message "Loaded ruby-mode")
;;           (setq ruby-indent-level 4)
;;           (flycheck-add-next-checker 'ruby '(warning . ruby-rubocop))
;;   )

;; ; Code navigation, documentation lookup and completion for Ruby
;; ; https://github.com/dgutov/robe
;; (use-package robe
;;   :ensure t
;;   :defer  t
;;   :after  (ruby-mode company)
;;   :config (message "Loaded robe")
;;           '(push 'company-robe company-backends)
;;           ;; (defadvice inf-ruby-console-auto (before activate-rvm-for-robe activate)
;;           ;;   (rvm-activate-corresponding-ruby))
;;   )

;; ; inf-ruby provides a REPL buffer connected to a Ruby subprocess.
;; ; https://github.com/nonsequitur/inf-ruby/
;; (use-package inf-ruby
;;   :ensure t
;;   :defer  t
;;   :after  ruby-mode
;;   :config (message "Loaded inf-ruby")
;;   )

; An enhanced ruby-mode for Emacs that uses Ripper in ruby 1.9+ to highlight and indent the source code
; https://github.com/zenspider/enhanced-ruby-mode
;; (use-package enh-ruby-mode
;;   :ensure t
;;   :init   (add-hook 'ruby-mode-hook 'enh-ruby-mode)
;;   :after  ruby-mode
;;   :config (message "Loaded enh-ruby-mode")
;;           ;; (add-hook 'ruby-mode-hook 'enh-ruby-mode)
;;           (setq enh-ruby-indent-level 4)
;;           (flycheck-add-next-checker 'ruby '(warning . ruby-rubocop))
;;   )

(provide 'programming-ruby)
;;; programming-ruby.el ends here
