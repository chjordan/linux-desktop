;;; init-built-in.el --- Customise built-in packages.

;;; Commentary:
;; This file just customises a bunch of built-in Emacs packages.  Some built-in
;; packages will be customised elsewhere (e.g. bibtex) as they are not
;; appropriate here.

;;; Code:
(message "Reading init-built-in.el")

(use-package ibuffer
  ;; :defer t
  :bind   ("C-x C-b" . ibuffer)
  :config (message "Loaded ibuffer")
  )
(use-package ediff
  :defer t
  :config (message "Loaded ediff")
          (setq ediff-window-setup-function (quote ediff-setup-windows-plain))
          (setq ediff-merge-split-window-function (quote split-window-horizontally))
          (setq ediff-split-window-function (quote split-window-horizontally))
  )
(use-package tramp
  :defer t
  :config (message "Loaded tramp")
          (setq tramp-auto-save-directory (concat (getenv "HOME") "/.emacs.d/auto-save-list"))
          ;; (setq tramp-default-method "rsync")
          ;; (setq tramp-default-method "ssh")
          ;; (setq tramp-use-ssh-controlmaster-options t)
          ;; (setq tramp-use-ssh-controlmaster-options nil)
          (setq tramp-ssh-controlmaster-options
                (concat
                 "-o ControlPath=/tmp/tramp_ssh_control_path_%%r@%%h:%%p "
                 "-o ControlMaster=auto -o ControlPersist=yes"
                 ))
          (setq tramp-copy-size-limit 1048576)
          (setq tramp-inline-compress-start-size nil)
          (setq tramp-remote-path (quote (tramp-own-remote-path tramp-default-remote-path)))
  )
(use-package re-builder
  :defer  t
  :config (message "Loaded re-builder")
          (setq reb-re-syntax 'pcre)
  )
(add-hook 'doc-view-mode-hook 'auto-revert-mode)
(use-package doc-view
  :defer t
  :config (message "Loaded doc-view")
          (setq doc-view-resolution 300)
  )
; https://emacs.stackexchange.com/a/2458
(defun modi/image-transform-fit-to-window()
  "Resize the image to fit the width or height based on the image and window ratios.
Imagemagick is required to run this function."
  (interactive)
  (let* ( (img-size (image-display-size (image-get-display-property) t))
          (img-width (car img-size))
          (img-height (cdr img-size))
          (img-h/w-ratio (/ (float img-height) (float img-width)))
          (win-width (- (nth 2 (window-inside-pixel-edges))
                        (nth 0 (window-inside-pixel-edges))))
          (win-height (- (nth 3 (window-inside-pixel-edges))
                         (nth 1 (window-inside-pixel-edges))))
          (win-h/w-ratio (/ (float win-height) (float win-width))))
    ;; Fit image by width if the h/w ratio of window is > h/w ratio of the image
    (if (> win-h/w-ratio img-h/w-ratio)
        (image-transform-fit-to-width)
      ;; Else fit by height
      (image-transform-fit-to-height))))
(use-package image-mode
  :defer  t
  :bind   (:map image-mode-map ("W" . modi/image-transform-fit-to-window))
  :config (message "Loaded image")
  )

(use-package hl-line
  :init   (global-hl-line-mode t)
  :config (message "Loaded hl-line")
          (defun my-hl-line-range-function ()
          (let ((lbp (line-beginning-position)))
            (cons (if (get-text-property lbp 'highlight-indent-guides-prop)
                      (save-excursion (back-to-indentation) (point))
                    lbp)
                  (line-beginning-position 2))))
          (setq hl-line-range-function 'my-hl-line-range-function)
  )

(provide 'init-built-in)
;;; init-built-in.el ends here
