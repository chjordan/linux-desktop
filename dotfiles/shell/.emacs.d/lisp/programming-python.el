;;; programming-python.el --- My emacs setup for python programming.

;;; Commentary:

;;; Code:
(message "Reading programming-python.el")

; Use pyls - https://github.com/palantir/python-language-server
; On arch, install: python-{language-server,rope,pyflakes,pycodestyle,pydocstyle} yapf
(use-package python
  :hook   (python-mode . lsp)
  :config (message "Loaded python-mode")
          (setq python-indent-offset 4)
          (setq lsp-pyls-plugins-pydocstyle-enabled t)
  )

;; (use-package python
;;   :config (add-hook 'python-mode-hook 'elpy-mode)
;;           (remove-hook 'elpy-modules 'elpy-module-flymake)
;;           (setq python-indent-offset 4)
;;           (setq python-shell-interpreter "ipython")
;;           (setq python-shell-interpreter-args "--simple-prompt --pprint")
;;           (setq flycheck-flake8rc "~/.config/flake8")
;;   )
;; (defun pytest ()
;;   (interactive)
;;   (let* ((project-root
;;           (replace-regexp-in-string "\n\\'" ""
;;                                     (shell-command-to-string "git rev-parse --show-toplevel")))
;;          (compile-command
;;           (concat "cd " project-root " && py.test --cov --cov-report=html"))
;;          )
;;     (message "printing project-root...")
;;     (message project-root)
;;     (message "printing compile-command...")
;;     (message compile-command)
;;     (compile compile-command)
;;     ))
;; ; Emacs Python Development Environment
;; ; https://github.com/jorgenschaefer/elpy
;; (use-package elpy
;;   :ensure t
;;   :defer  t
;;   :bind   (:map elpy-mode-map ("M-." . elpy-goto-definition)
;;                               ("C-x 4 M-." . elpy-goto-definition-other-window)
;;                 )
;;   ;; :bind   (:map elpy-mode-map ("C-c C-c" . elpy-shell-send-region-or-buffer-and-step)
;;   ;;                             ("C-c RET" . elpy-shell-send-current-statement))
;;   :config (message "Loaded elpy")
;;           (elpy-enable)
;;           (setq elpy-rpc-backend "jedi")
;;           (setq elpy-modules (quote
;;                               (elpy-module-sane-defaults
;;                                elpy-module-company
;;                                elpy-module-eldoc
;;                                elpy-module-highlight-indentation)))
;;                                ;; elpy-module-pyvenv
;;           ;; elpy-module-yasnippet)))
;;           (add-hook 'python-mode-hook (lambda() (local-unset-key (kbd "C-c C-c"))))
;;           (eval-after-load 'python-mode
;;             '(define-key elpy-mode-map (kbd "C-c C-c") elpy-shell-send-region-or-buffer-and-step)
;;             ;; ('(define-key elpy-mode-map "C-c C-c" elpy-shell-send-region-or-buffer))
;;             ;; ('(define-key elpy-mode-map "C-c RET" . elpy-shell-send-current-statement))
;;             )
;;   )
;; ; Company backend for Python jedi
;; ; https://github.com/syohex/emacs-company-jedi
;; (defun my/python-mode-hook ()
;;   (add-to-list 'company-backends 'company-jedi))
;; (use-package company-jedi
;;   :ensure t
;;   :defer  t
;;   :after  company
;;   :init   (add-hook 'python-mode-hook 'my/python-mode-hook)
;;   :config (message "Loaded company-jedi")
;;   )

(provide 'programming-python)
;;; programming-python.el ends here
