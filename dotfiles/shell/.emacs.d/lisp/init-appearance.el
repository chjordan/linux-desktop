;;; init-appearance.el --- Themes and cosmetics.

;;; Commentary:
;; General appearance customisation.

;;; Code:
(message "Reading init-appearance.el")
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)

; which-function-mode
; Display the name of the function that your cursor is currently visiting.
; Put this earlier in the mode line.
;; (let ((which-func '(which-func-mode ("" which-func-format " "))))
;;   (setq-default mode-line-format (remove which-func mode-line-format))
;;   (setq-default mode-line-misc-info (remove which-func mode-line-misc-info))
;;   (setq cell (last mode-line-format 8))
;;   (setcdr cell
;;           (cons which-func
;;                 (cdr cell))))

(which-function-mode 1)
; Put this earlier in the mode line.
(let ((which-func '(which-func-mode ("" which-func-format " "))))
  (setq mode-line-format (delete (assoc 'which-func-mode mode-line-format) mode-line-format))
  (setq mode-line-misc-info (delete (assoc 'which-function-mode mode-line-misc-info) mode-line-misc-info))
  (setq cell (last mode-line-format 8))
  (setcdr cell
          (cons which-func
                (cdr cell))))

(use-package monokai-theme
  :ensure t
  :config (message "Loaded monokai")
  )

;; (use-package doom-themes
;;   :ensure t
;;   :config (message "Loaded doom-themes")
;;   )

;; ; https://github.com/cfraz89/arc-dark-theme
;; (use-package arc-dark-theme
;;   :straight (:host github :repo "cfraz89/arc-dark-theme")
;;   :config (load-theme 'arc-dark t)
;;   )

; Emacs minor mode to highlight indentation
; https://github.com/DarthFennec/highlight-indent-guides
(use-package highlight-indent-guides
  :ensure   t
  :init     (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  :diminish highlight-indent-guides
  :config   (message "Loaded highlight-indent-guides")
            (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
            (setq highlight-indent-guides-responsive 'stack)
  )

; Emacs package for highlighting uncommitted changes
; https://github.com/dgutov/diff-hl
(use-package diff-hl
  :ensure t
  :init   (global-diff-hl-mode)
  :config (message "Loaded diff-hl")
  )

; This minor mode sets background color to strings that match color
; names, e.g. #0000ff is displayed in white with a blue background.
; https://elpa.gnu.org/packages/rainbow-mode.html
(use-package rainbow-mode
  :ensure t
  :defer  t
  :config (message "Loaded rainbow-mode")
  )

;; ; Emacs plugin aiming to become an aesthetic, modern looking tabs plugin
;; ; https://github.com/ema2159/centaur-tabs
;; (use-package centaur-tabs
;;   :ensure t
;;   :demand
;;   :bind   ("C-x ." . centaur-tabs-forward)
;;           ("C-x C-." . centaur-tabs-forward)
;;           ("C-x ," . centaur-tabs-backward)
;;           ("C-x C-," . centaur-tabs-backward)
;;   :config (setq centaur-tabs-style "alternate")
;;           (setq centaur-tabs-set-bar 'over)
;;           (setq centaur-tabs-set-close-button nil)
;;           (setq centaur-tabs-set-modified-marker t)
;;           (setq centaur-tabs-set-icons t)
;;           ;; (centaur-tabs-headline-match)
;;           ;; (centaur-tabs-inherit-tabbar-faces)
;;           (centaur-tabs-mode t)
;;           (centaur-tabs-group-by-projectile-project)
;;           (setq centaur-tabs-cycle-scope 'tabs)
;;           (defun centaur-tabs-buffer-groups ()
;;      "`centaur-tabs-buffer-groups' control buffers' group rules.

;;  Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
;;  All buffer name start with * will group to \"Emacs\".
;;  Other buffer group by `centaur-tabs-get-group-name' with project name."
;;      (list
;;       (cond
;; 	((or (string-equal "*" (substring (buffer-name) 0 1))
;; 	     (memq major-mode '(magit-process-mode
;; 				magit-status-mode
;; 				magit-diff-mode
;; 				magit-log-mode
;; 				magit-file-mode
;; 				magit-blob-mode
;; 				magit-blame-mode
;; 				)))
;; 	 "Emacs")
;; 	((derived-mode-p 'prog-mode)
;; 	 "Editing")
;; 	((derived-mode-p 'dired-mode)
;; 	 "Dired")
;; 	((memq major-mode '(helpful-mode
;; 			    help-mode))
;; 	 "Help")
;; 	((memq major-mode '(org-mode
;; 			    org-agenda-clockreport-mode
;; 			    org-src-mode
;; 			    org-agenda-mode
;; 			    org-beamer-mode
;; 			    org-indent-mode
;; 			    org-bullets-mode
;; 			    org-cdlatex-mode
;; 			    org-agenda-log-mode
;; 			    diary-mode))
;; 	 "OrgMode")
;; 	(t
;; 	 (centaur-tabs-get-group-name (current-buffer))))))
;;           )

(provide 'init-appearance)
;;; init-appearance.el ends here
