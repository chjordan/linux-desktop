;;; programming-go.el --- My emacs setup for julia programming.

;;; Commentary:
; lsp-julia is only available via quelpa, but, if allowed to run, quelpa is very
; annoying on startup, because it checks if melpa is up to date! For this
; reason, this file is not sourced at the moment.

;;; Code:
(message "Reading programming-julia.el")

(use-package quelpa
  :ensure t
  :config (message "Loaded quelpa")
  )

(quelpa
 '(quelpa-use-package
   :fetcher git
   :url "https://framagit.org/steckerhalter/quelpa-use-package.git"))
(require 'quelpa-use-package)

(use-package julia-mode
  :ensure t
  :defer  t
  :hook   (julia-mode . lsp)
  :config (message "Loaded julia-mode")
          (add-hook 'julia-mode-hook 'flycheck-mode)
  )

;; (use-package julia-repl
;;   :ensure t
;;   :defer  t
;;   :init   (add-hook 'julia-mode-hook 'julia-repl-mode)
;;   :after  julia-mode
;;   )

(use-package flycheck-julia
  :ensure t
  :defer  t
  :config (message "Loaded flycheck-rust")
          (add-hook 'flycheck-mode-hook #'flycheck-julia-setup)
  )

;; (quelpa '(lsp-julia :fetcher github :repo "non-Jedi/lsp-julia"))
(use-package lsp-julia
  :quelpa (lsp-julia :fetcher github :repo "non-Jedi/lsp-julia"))

;; (use-package julia-repl
;;   :ensure t
;;   :defer  t
;;   :init   (add-hook 'julia-mode-hook #'lsp-mode)
;;   :after  julia-mode
;;   )

(provide 'programming-julia)
;;; programming-julia.el ends here
