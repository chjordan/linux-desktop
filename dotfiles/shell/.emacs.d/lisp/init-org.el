;;; programming-c.el --- My emacs setup for C programming.

;;; Commentary:

;;; Code:
(message "Reading init-org.el")

; This doesn't update the statistics of the block which contained the original task!
(defun my/org-update-statistics ()
  (org-update-statistics-cookies t)

  ;; (interactive)
  ;; (let ((current-prefix-arg 4)) ;; emulate C-u
  ;;   (call-interactively 'org-update-statistics-cookies t)
  ;;   )

  ;; (message "updating first stats")
  ;; (org-update-statistics-cookies nil)
  ;; (message "moving up heading")
  ;; (outline-up-heading 1 t)
  ;; (message "updating second stats")
  ;; (org-update-statistics-cookies nil)
  ;; (message "popping")
  ;; (pop-to-mark-command)
  )

; https://orgmode.org/manual/Breaking-down-tasks.html
;; (defun org-summary-todo (n-done n-not-done)
;;   "Switch entry to DONE when all subentries are done, to TODO otherwise."
;;   (let (org-log-done org-log-states)   ; turn off logging
;;     (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to N-DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

; https://emacs.stackexchange.com/questions/19843/how-to-automatically-adjust-an-org-task-state-with-its-children-checkboxes
(defun my/org-checkbox-todo ()
  "Switch header TODO state to DONE when all checkboxes are ticked, to TODO otherwise."
  (let ((todo-state (org-get-todo-state)) beg end)
    (unless (not todo-state)
      (save-excursion
    (org-back-to-heading t)
    (setq beg (point))
    (end-of-line)
    (setq end (point))
    (goto-char beg)
    (if (re-search-forward "\\[\\([0-9]*%\\)\\]\\|\\[\\([0-9]*\\)/\\([0-9]*\\)\\]"
                   end t)
        (if (match-end 1)
        (if (equal (match-string 1) "100%")
            (unless (string-equal todo-state "DONE")
              (org-todo 'done))
          (unless (string-equal todo-state "TODO")
            (org-todo 'todo)))
          (if (and (> (match-end 2) (match-beginning 2))
               (equal (match-string 2) (match-string 3)))
          (unless (string-equal todo-state "DONE")
            (org-todo 'done))
        (unless (string-equal todo-state "TODO")
          (org-todo 'todo)))))))))

(use-package org
  :config (message "Loaded org-mode")
          (setq org-startup-indented t)
	  (add-hook 'org-after-refile-insert-hook 'my/org-update-statistics)
	  (add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
          (add-hook 'org-checkbox-statistics-hook 'my/org-checkbox-todo)
  )

  ;; :init   (add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
  ;;         (add-hook 'org-checkbox-statistics-hook 'my/org-checkbox-todo)
  ;; 	  (add-hook 'org-after-refile-insert-hook 'org-update-checkbox-count t)

;; 	  ;; (add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
;;           ;; (add-hook 'org-checkbox-statistics-hook 'my/org-checkbox-todo)
;; 	  ;; (add-hook 'org-after-refile-insert-hook 'org-update-checkbox-count t)

; A simple org-mode based journaling mode
; https://github.com/bastibe/org-journal
(use-package org-journal
  :ensure t
  :config (message "Loaded org-journal")
          (add-to-list 'auto-mode-alist '("\\.orgj\\'" . org-journal-mode))
  :custom (org-journal-dir (concat (getenv "HOME") "/org-mode-files/diary"))
          (org-journal-file-format "%Y%m%d.orgj")
          (org-journal-date-format "%A, %d %B %Y")
  )

(provide 'init-org)
;;; init-org.el ends here
