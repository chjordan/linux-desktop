;;; init-ivy.el --- My customisations for ivy and friends.

;;; Commentary:
;; I have no idea why ivy is associated with swiper and counsel, but they're all
;; connected, so they should be together.

;;; Code:
(message "Reading init-ivy.el")

(use-package ivy
  :ensure  t
  :delight ivy-mode
  :bind    ("C-x b" . ivy-switch-buffer)
  :config  (message "Loaded ivy")
           (ivy-mode t)
           (setq ivy-wrap t)
           (setq ivy-height 15)
           (setq ivy-use-virtual-buffers t)
           (setq ivy-count-format "(%d/%d) ")
  )
(use-package swiper
  :ensure t
  :bind   ("C-s" . counsel-grep-or-swiper)
          ("C-r" . counsel-grep-or-swiper)
  :config (message "Loaded swiper")
  )
;; Not including smex causes M-x commands to be ordered alphabetically, rather
;; than by last use. Is it possible to just use the ivy suite instead?
(use-package smex
  :ensure t
  )
(use-package counsel
  :ensure t
  :demand
  :bind   ("M-x" . counsel-M-x)
          ("C-x C-f" . counsel-find-file)
          ("<f1> f" . counsel-describe-function)
          ("<f1> v" . counsel-describe-variable)
          ("<f1> l" . counsel-find-library)
          ("<f2> i" . counsel-info-lookup-symbol)
          ("<f2> u" . counsel-unicode-char)
  :config (message "Loaded counsel")
          ; Ignore dotfiles. To find them, start input with a dot.
          (setq counsel-find-file-ignore-regexp "\\`\\.")
  )
;; (use-package counsel-tramp
;;   :ensure t
;;   :defer  t
;;   :bind   ("C-c c s" . counsel-tramp)
;;   :config (message "Loaded counsel-tramp")
;;           ;; (add-hook 'counsel-tramp-pre-command-hook '(lambda () (global-aggressive-indent-mode 0)
;; 		  ;;   	                                       (/projectile-mode 0)
;; 		  ;;   	                                       (editorconfig-mode 0))
;;           ;;           )
;;           ;; (add-hook 'counsel-tramp-quit-hook '(lambda () (global-aggressive-indent-mode 1)
;; 		  ;;                                       (projectile-mode 1)
;; 		  ;;                                       (editorconfig-mode 1))
;;           ;;           )
;;           )
;; (use-package ivy-hydra
;;   :ensure t
;;   :config (message "Loaded ivy-hydra")
;;   )

(provide 'init-ivy)
;;; init-ivy.el ends here
