;;; init-navigation.el --- Various packages for navigation.

;;; Commentary:
;; Packages are ordered by importance/usage.

;;; Code:
(message "Reading init-navigation.el")

(use-package avy
  :ensure t
  :defer  t
  :bind   ("M-g w" . avy-goto-char-timer)
          ("M-g c" . avy-goto-char)
          ("M-g l" . avy-goto-line)
  :config (message "Loaded avy")
  ;; :chords ("jj" . avy-goto-word-1)
  ;;         ("jk" . avy-goto-char)
  ;;         ("jl" . avy-goto-line)
  )
; Make operations on words more fine-grained
; https://github.com/emacsmirror/syntax-subword
(use-package syntax-subword
  :ensure t
  :init   (global-syntax-subword-mode t)
  :config (message "Loaded syntax-subword")
  )
(use-package browse-kill-ring
  :ensure t
  :defer  t
  :bind   ("C-c y" . browse-kill-ring)
  :config (message "Loaded browse-kill-ring")
  )
(use-package undo-tree
  :ensure  t
  :init    (global-undo-tree-mode t)
  :delight undo-tree-mode
  ;; :chords  ("UU" . undo-tree-visualize)
  :bind    ("C-c u" . undo-tree-visualize)
  :config  (message "Loaded undo-tree")
  )
(use-package expand-region
  :ensure t
  :defer  t
  :bind   ("C-=" . er/expand-region)
  :config (message "Loaded expand-region")
  )
(use-package dumb-jump
  :ensure t
  :defer  t
  :init   (add-hook 'prog-mode-hook 'dumb-jump-mode)
  :bind   ("C-M-S-g" . dumb-jump-back)
  :config (message "Loaded dumb-jump")
          (setq dumb-jump-selector 'ivy)
          (global-unset-key (kbd "C-M-j"))
          (define-key dumb-jump-mode-map (kbd "C-M-p") nil)
  )

(provide 'init-navigation)
;;; init-navigation.el ends here
