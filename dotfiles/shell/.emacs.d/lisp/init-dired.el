;;; init-dired.el --- My emacs setup for dired.

;;; Commentary:

;;; Code:
(message "Reading init-dired.el")

(use-package dired
  :after  diff-hl
  :config (message "Loaded dired")
          (setq dired-dwim-target t)
          (setq dired-listing-switches "-alh")
          (add-hook 'dired-mode-hook 'diff-hl-dired-mode-unless-remote)
          ;; (use-package dired-quick-sort
          ;;   :ensure t
          ;;   :config (message "Loaded dired-quick-sort")
          ;;           (add-hook 'dired-mode-hook (lambda () (dired-quick-sort-setup)))
          ;;   )
          (use-package dired-subtree
            :ensure t
            :init (bind-keys :map dired-mode-map
                             ("i" . dired-subtree-insert)
                             (";" . dired-subtree-remove)
                             ("C-c" . dired-subtree-cycle)
                             ("C-n" . dired-subtree-next-sibling)
                             ("C-p" . dired-subtree-previous-sibling)
                             )
                             ;; :prefix "C-,"
                             ;; :prefix-map dired-subtree-map
                             ;; :prefix-docstring "Dired subtree map."
                             ;; ("<C-i-key>" . dired-subtree-insert)
                             ;; ("C-/" . dired-subtree-apply-filter)
                             ;; ("C-k" . dired-subtree-remove)
                             ;; ("C-n" . dired-subtree-next-sibling)
                             ;; ("C-p" . dired-subtree-previous-sibling)
                             ;; ("C-u" . dired-subtree-up)
                             ;; ("C-d" . dired-subtree-down)
                             ;; ("C-a" . dired-subtree-beginning)
                             ;; ("C-e" . dired-subtree-end)
                             ;; ("C-c" . dired-subtree-cycle)
                             ;; ("m" . dired-subtree-mark-subtree)
                             ;; ("u" . dired-subtree-unmark-subtree)
                             ;; ("C-o C-f" . dired-subtree-only-this-file)
                             ;; ("C-o C-d" . dired-subtree-only-this-directory))
            :config (message "Loaded dired-subtree")
                    ;; (add-hook 'dired-mode-hook (lambda () (dired-quick-sort-setup)))
            )
          (use-package dired-rainbow
            :ensure t
            :config (message "Loaded dired-rainbow")
                    (dired-rainbow-define html "#4e9a06" ("htm" "html" "xhtml"))
                    (dired-rainbow-define media "#ce5c00" ("mp3" "mp4" "avi" "mpg" "flv" "ogg" "mkv"))
                    ; highlight executable files, but not directories
                    (dired-rainbow-define-chmod executable-unix "Green" "-[rw-]+x.*")
            )
          ;; (use-package dired-collapse
          ;;   :ensure t
          ;;   :config (message "Loaded dired-collapse")
          ;;           (add-hook 'dired-mode-hook (lambda () (dired-collapse-mode 1)))
          ;;   )
          (use-package dired-du
            :ensure t
            :config (message "Loaded dired-du")
            )
          (use-package dired-narrow
            :ensure t
            :bind (:map dired-mode-map ("/" . dired-narrow))
            :config (message "Loaded dired-narrow")
            )
  )

(provide 'init-dired)
;; What does this do??
(put 'dired-find-alternate-file 'disabled nil)
;;; init-dired.el ends here
