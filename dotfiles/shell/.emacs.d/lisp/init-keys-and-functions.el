;;; init-keys-and-functions.el --- My custom keybindings and functions.

;;; Commentary:
;; Mostly stolen from others.

;;; Code:
(message "Reading init-keys-and-functions.el")

(defun back-window ()
  "Provides a back equivalent to 'other-window'."
  (interactive)
  (other-window -1)
  )

; http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun smarter-move-beginning-of-line (arg)
  (interactive "^p")
  (setq arg (or arg 1))
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))
  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

; http://www.emacswiki.org/emacs/CopyingWholeLines
;; (defun yank-with-comment (&optional arg)
(defun yank-with-comment (arg)
  "Copy the current line, comment it, then paste it beneath the current line."
  (interactive "p")
  (save-excursion
    (copy-region-as-kill
     (line-beginning-position)
     (progn (if arg (forward-visible-line arg)
              (end-of-visible-line))
            (point)
            )))
  (comment-or-uncomment-region (line-beginning-position) (line-end-position))
  (move-end-of-line 1)
  ;; (newline)
  ;; (open-line)
  (next-line)
  (move-beginning-of-line 1)
  (yank)
  (forward-line -1)
  )

(defun sudo-dired ()
  "Login as sudo via tramp."
  (interactive)
  (dired "/sudo::/"))
(defun sudo-open-this-file ()
  "Reopen the current file as root, preserving point position."
  (interactive)
  (let ((p (point)))
    ;; (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))
    (find-alternate-file (concat "/sudo::" buffer-file-name))
    ;; (find-alternate-file (concat "/sudoedit:root@localhost:" buffer-file-name))
    (goto-char p)))
(defun tramp-ssh-open-this-file ()
  "Reopen the current file with the 'ssh' method, preserving point position."
  (interactive)
  (let ((p (point)))
    (find-alternate-file (concat "/ssh:"
                                 (file-remote-p default-directory 'host)
                                 ":"
                                 (car (last (split-string (buffer-file-name) ":")))))
    (goto-char p)))
(defun tramp-rsync-open-this-file ()
  "Reopen the current file with the 'rsync' method, preserving point position."
  (interactive)
  (let ((p (point)))
    (find-alternate-file (concat "/rsync:"
                                 (file-remote-p default-directory 'host)
                                 ":"
                                 (car (last (split-string (buffer-file-name) ":")))))
    (goto-char p)))

; http://pragmaticemacs.com/emacs/dont-kill-buffer-kill-this-buffer-instead/
(defun bjm/kill-this-buffer ()
  "Kill the current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

; https://stackoverflow.com/a/11719472
(defun scratch-create-buffer nil
   "Create a scratch buffer."
   (interactive)
   (switch-to-buffer (get-buffer-create "*scratch*"))
   (lisp-interaction-mode))

; https://emacs.stackexchange.com/questions/2606/opposite-of-fill-paragraph
(defun unfill-paragraph ()
  "Take a multi-line paragraph and make it into a single line of text."
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))
(define-key global-map "\M-Q" 'unfill-paragraph)

(global-set-key (kbd "C-x .")   'next-buffer)
(global-set-key (kbd "C-x C-.") 'next-buffer)
(global-set-key (kbd "C-x ,")   'previous-buffer)
(global-set-key (kbd "C-x C-,") 'previous-buffer)
(global-set-key (kbd "C-x C-o") 'back-window)
(global-set-key (kbd "C-h")     'backward-delete-char)

(global-set-key (kbd "C-M-h")   'backward-kill-word)
(global-set-key (kbd "C-M-k")   'yank-with-comment)
(global-set-key (kbd "C-a")     'smarter-move-beginning-of-line)
(global-set-key (kbd "C-M-y")   'clipboard-yank)

(global-set-key (kbd "C-x k")   'bjm/kill-this-buffer)
(global-set-key (kbd "C-x M-k") 'kill-buffer-and-window)

(global-set-key (kbd "C-M-|")   'indent-rigidly)

(provide 'init-keys-and-functions)
;;; init-keys-and-functions.el ends here
