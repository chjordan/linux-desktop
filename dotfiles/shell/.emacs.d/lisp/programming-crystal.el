;;; programming-crystal.el --- My emacs setup for crystal programming.

;;; Commentary:
;; I still have not really used crystal, so I don't know how well this all
;; works.

;;; Code:
(message "Reading programming-crystal.el")

; A minimal crystal mode for emacs, based on ruby-mode. https://melpa.org/#/crystal-mode
; https://github.com/crystal-lang-tools/emacs-crystal-mode
(use-package crystal-mode
  :ensure t
  :defer  t
  :init (add-to-list 'auto-mode-alist '("\\.cr$" . crystal-mode))
        (add-to-list 'interpreter-mode-alist '("crystal" . crystal-mode))
        (add-hook 'crystal-mode-hook 'flycheck-mode)
  :after (flycheck-crystal)
  :config (message "Loaded crystal-mode")
  )

(use-package flycheck-crystal
  :ensure t
  :config (message "Loaded flycheck-crystal")
  )

(provide 'programming-crystal)
;;; programming-crystal.el ends here
