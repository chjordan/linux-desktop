;;; programming-latex.el --- My emacs setup for LaTeX programming.

;;; Commentary:
;; This one is quite painful.  The usual "Loaded auctex" message in :config
;; doesn't work here, and if you actually want to use auctex, you need to
;; install it manually.

;;; Code:
(message "Reading programming-latex.el")

(use-package auctex
  :mode ("\\.tex\\'" . TeX-latex-mode)
  )

; https://emacs.stackexchange.com/questions/42330/automatic-latex-preview-pane-with-auctex-and-or-mode-latex-export
(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer) ;; revert pdf after compile
(setq-default TeX-engine 'xetex)
(setq TeX-source-correlate-mode t)
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))) ;; use pdf-tools for viewing
(add-hook 'TeX-mode-hook (lambda () (setq show-trailing-whitespace t)))

; https://tex.stackexchange.com/questions/50827/a-simpletons-guide-to-tex-workflow-with-emacs
(add-hook 'TeX-mode-hook 'LaTeX-math-mode)
(add-hook 'TeX-mode-hook 'turn-on-reftex)

;; ;; Using digestif. Sadly, not as nice as auctex.
;; (use-package tex-mode
;;   :ensure t
;;   :defer  t
;;   :hook   (latex-mode . lsp)
;;   :config (message "Loaded latex-mode")
;;           (add-hook 'latex-mode-hook 'flycheck-mode)
;;           (flycheck-add-next-checker 'lsp-ui '(error . tex-lacheck))
;;           (flycheck-add-next-checker 'lsp-ui '(error . tex-chktex))
;;           (setq latex-run-command "xelatex")

;;           ; https://emacs.stackexchange.com/questions/42330/automatic-latex-preview-pane-with-auctex-and-or-mode-latex-export
;;           (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer) ;; revert pdf after compile
;;           (setq-default TeX-engine 'xetex)
;;           (setq TeX-source-correlate-mode t)
;;           (setq TeX-view-program-selection '((output-pdf "PDF Tools"))) ;; use pdf-tools for viewing
;;           (add-hook 'TeX-mode-hook (lambda () (setq show-trailing-whitespace t)))
;;   )

;; (use-package latex-preview-pane
;;   :ensure t
;;   :defer  t
;;   :config (message "Loaded latex-preview-pane")
;;           (setq pdf-latex-command "xelatex")
;;           ;; (latex-preview-pane-enable)
;;   )

(provide 'programming-latex)
;;; programming-latex.el ends here
