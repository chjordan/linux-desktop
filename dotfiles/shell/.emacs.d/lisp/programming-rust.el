;;; programming-rust.el --- My emacs setup for rust programming.

;;; Commentary:

;;; Code:
(message "Reading programming-rust.el")

;; https://www.reddit.com/r/rust/comments/a3da5g/my_entire_emacs_config_for_rust_in_fewer_than_20/

; https://github.com/rust-lang/rls
; Run: rustup component add rls rust-analysis rust-src rustfmt clippy
(use-package rust-mode
  :ensure t
  :defer  t
  :hook   (rust-mode . lsp)
  :bind   (:map rust-mode-map ("C-c s" . rust-format-buffer))
  :config (message "Loaded rust-mode")
          (add-hook 'rust-mode-hook 'flycheck-mode)
          ;; I'm using lsp-ui here, but I'm not sure if that should be the main
          ;; flycheck checker!
          (flycheck-add-next-checker 'lsp-ui '(warning . rust-clippy))
  )

(use-package flycheck-rust
  :ensure t
  :defer  t
  :config (message "Loaded flycheck-rust")
          (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
  )

(use-package cargo
  :ensure t
  :defer  t
  :diminish cargo-minor-mode
  :hook (rust-mode . cargo-minor-mode)
  :config (message "Loaded cargo")
  )

(provide 'programming-rust)
;;; programming-rust.el ends here
