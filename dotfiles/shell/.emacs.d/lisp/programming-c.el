;;; programming-c.el --- My emacs setup for C programming.

;;; Commentary:

;;; Code:
(message "Reading programming-c.el")

(use-package cc-mode
  :defer  t
  :hook   ((c-mode c++-mode) . lsp)
  :config (message "Loaded cc-mode")
          (setq c-basic-offset 4)
          ;; lsp-ui set itself as the main flycheck checker, but doesn't seem to
          ;; automatically run clang or gcc. Add them here. This ordering means
          ;; that gcc comes before clang.
          (flycheck-add-next-checker 'lsp-ui '(error . c/c++-clang))
          (flycheck-add-next-checker 'lsp-ui '(error . c/c++-gcc))

          ;; (setq flycheck-gcc-warnings '("all" "extra" "pedantic" "strict-prototypes" "missing-prototypes" "missing-declarations"))
          ;; (setq flycheck-clang-warnings '("all" "extra" "pedantic" "strict-prototypes" "missing-prototypes" "missing-declarations"))
  )

(use-package cuda-mode
  :ensure t
  :defer  t
  :config (message "Loaded cuda-mode")
          (setq flycheck-cuda-include-path (quote ("." "/opt/cuda/include")))
          (setq flycheck-cuda-language-standard "c++11")
  )

(provide 'programming-c)
;;; programming-c.el ends here
