;;; programming-lsp.el --- My emacs setup for the language server protocol.

;;; Commentary:

;;; Code:
(message "Reading programming-lsp.el")

(use-package lsp-mode
  :ensure t
  :commands lsp
  :config (message "Loaded lsp-mode")
          (require 'lsp-clients)
          (setq lsp-prefer-flymake nil)
  )

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config (message "Loaded lsp-ui")
  )

(use-package company-lsp
  :ensure t
  :config (message "Loaded company-lsp")
  )

(require 'lsp-ui-flycheck)
(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-after-open-hook (lambda () (lsp-ui-flycheck-enable 1))))

(provide 'programming-lsp)
;;; programming-lsp.el ends here
