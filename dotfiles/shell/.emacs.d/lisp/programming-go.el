;;; programming-go.el --- My emacs setup for go programming.

;;; Commentary:

;;; Code:
(message "Reading programming-go.el")

(use-package go-mode
  :ensure t
  :defer  t
  )

(provide 'programming-go)
;;; programming-go.el ends here
