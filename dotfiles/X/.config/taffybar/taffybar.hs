{-# LANGUAGE PackageImports #-}
module Main where

import Data.Maybe (fromJust, isNothing)
import Data.IORef

import System.Taffybar
import System.Taffybar.FreedesktopNotifications
import System.Taffybar.SimpleClock
import System.Taffybar.Systray
import System.Taffybar.Pager hiding (colorize)
import System.Taffybar.TaffyPager
import System.Taffybar.Widgets.PollingLabel
import System.Taffybar.Widgets.PollingBar
import System.Taffybar.Widgets.PollingGraph
-- import System.Taffybar.WorkspaceSwitcher
-- import System.Taffybar.WorkspaceHUD
import System.Taffybar.WorkspaceHUD

import Text.Printf
import qualified "gtk3" Graphics.UI.Gtk as Gtk
-- import "gtk3" Graphics.UI.Gtk

import qualified System.Taffybar.Custom.Widget as CW
import System.Taffybar.Custom.Util
import System.Taffybar.Custom.CPU
import System.Taffybar.Custom.Memory
import System.Taffybar.Custom.Battery
import System.Taffybar.Custom.Network


-- import StatusNotifier.Tray
-- import System.Taffybar.Widget.SNITray


defaultFont :: String
defaultFont = "Terminus (TTF) 9"
bgColour :: String
bgColour = "#282a2e"
fgColour :: String
fgColour = "#bbbbbb"
textColour :: String
textColour = "#eeeeee"

-- | Creates markup with the given foreground and background colors and the
-- given contents.
colorize :: String -- ^ Foreground colour.
         -> String -- ^ Background colour.
         -> String -- ^ Contents.
         -> String
-- colorize fg bg = printf "<span font='Inconsolata 12' %s%s>%s</span>" (attr "fg" fg) (attr "bg" bg)
colorize fg bg = printf ("<span font='" ++ defaultFont ++ "' %s%s>%s</span>") (attr "fg" fg) (attr "bg" bg)
  where attr name value
          | null value = ""
          | otherwise  = printf " %scolor=\"%s\"" name value


-- xmonad logging, workspaces, etc
expertLog :: IO Gtk.Widget
-- expertLog = taffyPagerNew defaultPagerConfig { --activeWindow     = colorize fgColour bgColour defaultFont . escape -- . shorten 40
--                                              activeLayout     = colorize fgColour bgColour defaultFont . escape
--                                              , activeWorkspace  = colorize textColour "#990000" defaultFont . escape
--                                              , hiddenWorkspace  = colorize fgColour bgColour defaultFont . escape
--                                              , emptyWorkspace   = const ""
--                                              , visibleWorkspace = wrap "(" ")" . escape
--                                              , urgentWorkspace  = colorize "red" "yellow" defaultFont . escape
--                                              , widgetSep        = " : "
--                                              }
-- expertLog = taffyPagerNew defaultPagerConfig { activeWindow     = escape
--                                              , activeLayout     = colorize fgColour bgColour . escape
--                                              , activeWorkspace  = colorize textColour "#990000" . wrap " " " " . escape
--                                           -- , hiddenWorkspace  = wrap " " " " . escape
--                                           -- , workspacePad = False
--                                              }
-- expertLog = taffyPagerNew defaultWorkspaceHUDConfig { activeWindow     = escape
--                                                     , activeLayout     = colorize fgColour bgColour . escape
--                                                     , activeWorkspace  = colorize textColour "#990000" . wrap " " " " . escape
--                                                     -- , hiddenWorkspace  = wrap " " " " . escape
--                                                     -- , workspacePad = False
--                                                     }
expertLog = taffyPagerHUDNew customPagerConfig customWorkspaceHUDConfig
  where
    customPagerConfig = defaultPagerConfig { activeWindow     = escape
                                           , activeLayout     = colorize fgColour bgColour . escape
                                           , activeWorkspace  = colorize textColour "#990000" . wrap " " " " . escape
                                           -- , emptyWorkspace   = const ""
                                        -- , hiddenWorkspace  = wrap " " " " . escape
                                        -- , workspacePad = False
                                           }
    -- customWorkspaceHUDConfig = defaultWorkspaceHUDConfig
    customWorkspaceHUDConfig = defaultWorkspaceHUDConfig { maxIcons = Just 0
                                                         , underlinePadding = 10
                                                         }

-- separatorIcon :: IO Widget
-- separatorIcon = staticWidget $ sijiIcon "\xE1AC"

cpuIcon :: IO Gtk.Widget
cpuIcon = staticWidget $ sijiIcon "\xE026"

netCallback :: IORef [Int] -> Double -> IO String
netCallback netIORef pollSeconds = do
  primary <- primaryInterface
  [rx', tx'] <- interfaceRates primary netIORef pollSeconds
  let [rxRate, txRate] = formatRates [rx', tx']
  return $ if isNothing primary
           then ""
           else unwords [ ""
                        , sijiIcon "\xE1AC"
                        , fromJust primary
                        , sijiIcon "\xE061"
                        , rxRate
                        , sijiIcon "\xE060"
                        , txRate
                        ]

main :: IO ()
main = do
  cpuLoadIORef <- newIORef ([0, 0, 0] :: [Int])
  cpuRateIORef <- newIORef ([0, 0] :: [Double])
  netIORef <- newIORef ([0, 0] :: [Int])

  let pager = expertLog
      -- note = notifyAreaNew defaultNotificationConfig
      -- wss = wspaceSwitcherNew pager

      cpuTextWidget = CW.textWidget "%" 2 (cpuText cpuLoadIORef cpuRateIORef)
      cpuCfg = defaultGraphConfig { graphPadding = 0
                                  , graphBorderColor = (0.16, 0.16, 0.18)
                                  , graphDataColors = [ (0, 1, 0, 1)
                                                      , (0, 0, 1, 1)
                                                      ]
                                  }
      cpuGraphWidget = pollingGraphNew cpuCfg 2 (readIORef cpuRateIORef)

      memCfg = defaultGraphConfig { graphPadding = 0
                                  , graphBorderColor = (0.16, 0.16, 0.18)
                                  , graphDataColors = [(1, 0, 0, 1)]
                                  }
      memTextWidget = CW.textWidget "%" 2 memText
      memGraphWidget = pollingGraphNew memCfg 2 memCallback

      batteryWidget = CW.textWidget "" 3 batteryInfo

      netTextWidget = CW.textWidget "%" 2 (netCallback netIORef 2)

      clock = textClockNew Nothing text 5
        where text = unwords [ ""
                             , sijiIcon "\xE1AC"
                             , sijiIcon "\xE015"
                             , "<span font='" ++ defaultFont
                               ++ "' fgcolor='" ++ fgColour
                               ++ "'>%a %d/%m %H:%M</span>"
                             , sijiIcon "\xE1AC"
                             , "" ]

      oldTray = systrayNew
      -- the following doesn't seem to do anything
      -- https://github.com/travitch/taffybar/issues/162
      -- tray = do
      --   tray <- systrayNew
      --   container <- Gtk.eventBoxNew
      --   Gtk.containerAdd container tray
      --   Gtk.widgetSetName container "Taffytray"
      --   Gtk.widgetSetName tray "Taffytray"
      --   return $ Gtk.toWidget container

  defaultTaffybar defaultTaffybarConfig { barHeight = 16
                                        , barPosition = Top
                                        , widgetSpacing = 0
                                        , startWidgets = [ pager ]
                                        , endWidgets = reverse [ cpuTextWidget
                                                               , cpuGraphWidget
                                                               , memTextWidget
                                                               , memGraphWidget
                                                               , batteryWidget
                                                               , netTextWidget
                                                               , clock
                                                               -- , makeContents sniTrayNew
                                                               -- , oldTray
                                                               ]
                                        }
