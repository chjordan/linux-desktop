import System.IO
import System.Exit
import Data.List (intercalate)
import qualified Data.Map as M
-- import Control.Monad (liftM, sequence)

import Network.HostName (getHostName)

import XMonad
import qualified XMonad.StackSet as W
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders
import XMonad.Layout.ThreeColumns
import qualified XMonad.Layout.IndependentScreens as IS
import XMonad.Actions.CycleWS
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.Loggers
import XMonad.Util.NamedWindows (getName, unName)
import Graphics.X11.ExtraTypes.XF86

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import qualified DBus.Client as DBus
-- import System.Taffybar.XMonadLog (dbusLog)

-- import XMonad.Util.EZConfig
-- import qualified XMonad.Actions.PhysicalScreens as PS

import qualified XMonad.Hooks.EwmhDesktops as E

-- import Control.Monad
-- import Control.Applicative
-- import Control.Concurrent

-- https://github.com/vicfryzel/xmonad-config/blob/master/xmonad.hs

-- xprop
-- myManageHook = composeAll
--   [ className =? "stalonetray"    --> doIgnore
--   , title     =? "PGPLOT Server"  --> doFloat
--   , className =? "Qalculate-gtk"  --> doFloat
--   , className =? "Kvis"           --> doFloat
--   -- , isFullscreen                  --> (doF W.focusDown <+> doFullFloat)]
--   , isFullscreen                  --> doFullFloat
--   ]
myManageHook = (isFullscreen --> doFullFloat) <+> manageDocks

isOnScreen :: ScreenId -> WindowSpace -> Bool
isOnScreen s ws = s == IS.unmarshallS (W.tag ws)

currentScreen :: X ScreenId
currentScreen = gets (W.screen . W.current . windowset)

spacesOnCurrentScreen :: WSType
spacesOnCurrentScreen = WSIs (isOnScreen <$> currentScreen)

-- /usr/include/X11/keysymdef.h
myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@XConfig {XMonad.modMask = modMask} = M.fromList $
  [ ((modMask .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
  , ((modMask, xK_r),
      spawn "dmenu_run -m 0 -fn \"Terminus (TTF):pixelsize=12:antialias=true:autohint=true\" -nb \"#222222\" -nf \"#bbbbbb\" -sb \"#990000\" -sf \"#eeeeee\"")
  , ((modMask, xK_F12),                                   spawn "xscreensaver-command -lock")

  , ((modMask, xK_Print),                                 spawn "scrot")
  , ((modMask .|. shiftMask, xK_Print),                   spawn "scrot -s")

  , ((modMask, xK_e),                                     spawn "emacsclient -c -a=")
  , ((modMask, xK_p),                                     spawn "pavucontrol")
  , ((modMask, xK_q),                                     spawn "qalculate-gtk")
  , ((modMask, xK_w),                                     spawn "spacefm")

  , ((0, xF86XK_AudioLowerVolume),                        spawn "pulseaudio-ctl down")
  , ((0, xF86XK_AudioRaiseVolume),                        spawn "pulseaudio-ctl up")
  , ((0, xF86XK_AudioMute),                               spawn "pulseaudio-ctl mute")
  , ((0, xF86XK_AudioMicMute),                            spawn "pulseaudio-ctl mute-input")

  , ((0, xF86XK_MonBrightnessDown),                       spawn "xbacklight -dec 5")
  , ((0, xF86XK_MonBrightnessUp),                         spawn "xbacklight -inc 5")
  , ((modMask .|. mod1Mask, xK_Down),                     spawn "mpc toggle")
  , ((modMask .|. mod1Mask, xK_Left),                     spawn "mpc prev")
  , ((modMask .|. mod1Mask, xK_Right),                    spawn "mpc next")
  , ((modMask .|. controlMask .|. mod1Mask, xK_Left),     spawn "mpc seek -")
  , ((modMask .|. controlMask .|. mod1Mask, xK_Right),    spawn "mpc seek +")
  , ((modMask .|. mod1Mask, xK_minus),                    spawn "mpc volume -2")
  , ((modMask .|. mod1Mask, xK_equal),                    spawn "mpc volume +2")

  -- "Standard" xmonad key bindings
  -- Close focused window.
  , ((modMask .|. shiftMask, xK_c),                       kill)
  -- Cycle through the available layout algorithms.
  , ((modMask, xK_space),                                 sendMessage NextLayout)
  -- , ((modMask .|. shiftMask, xK_space),                   sendMessage PrevLayout)
  --  Reset the layouts on the current workspace to default.
  , ((modMask .|. shiftMask, xK_space),                   setLayout $ XMonad.layoutHook conf)
  -- Resize viewed windows to the correct size.
  , ((modMask, xK_n),                                     refresh)
  -- Move focus to the next window.
  , ((modMask, xK_Tab),                                   windows W.focusDown)
  -- Move focus to the next/previous window.
  , ((modMask, xK_j),                                     windows W.focusDown)
  , ((modMask, xK_k),                                     windows W.focusUp)
  -- Move focus to the master window.
  , ((modMask, xK_m),                                     windows W.focusMaster)
  -- Swap the focused window and the master window.
  , ((modMask, xK_Return),                                windows W.swapMaster)
  -- Swap the focused window with the next/previous window.
  , ((modMask .|. shiftMask, xK_j),                       windows W.swapDown)
  , ((modMask .|. shiftMask, xK_k),                       windows W.swapUp)
  -- Shrink/expand the master area.
  , ((modMask, xK_h),                                     sendMessage Shrink)
  , ((modMask, xK_l),                                     sendMessage Expand)
  -- Push window back into tiling.
  , ((modMask, xK_t),                                     withFocused $ windows . W.sink)
  -- Increment/decrement the number of windows in the master area.
  , ((modMask, xK_i),                                     sendMessage (IncMasterN 1))
  , ((modMask, xK_d),                                     sendMessage (IncMasterN (-1)))

  -- , ((modMask, xK_Left),                                  prevWS)
  -- , ((modMask, xK_Right),                                 nextWS)
  -- , ((modMask, xK_comma),                                 prevWS)
  -- , ((modMask, xK_period),                                nextWS)
  -- , ((modMask .|. shiftMask, xK_Left),                    mconcat [shiftToPrev, prevWS])
  -- , ((modMask .|. shiftMask, xK_Right),                   mconcat [shiftToNext, nextWS])
  -- , ((modMask .|. shiftMask, xK_comma),                   mconcat [shiftToPrev, prevWS])
  -- , ((modMask .|. shiftMask, xK_period),                  mconcat [shiftToNext, nextWS])
  , ((modMask, xK_Left),                                  moveTo Prev spacesOnCurrentScreen)
  , ((modMask, xK_Right),                                 moveTo Next spacesOnCurrentScreen)
  , ((modMask, xK_comma),                                 moveTo Prev spacesOnCurrentScreen)
  , ((modMask, xK_period),                                moveTo Next spacesOnCurrentScreen)
  , ((modMask .|. shiftMask, xK_Left),                    mconcat [shiftToPrev, moveTo Prev spacesOnCurrentScreen])
  , ((modMask .|. shiftMask, xK_Right),                   mconcat [shiftToNext, moveTo Next spacesOnCurrentScreen])
  , ((modMask .|. shiftMask, xK_comma),                   mconcat [shiftToPrev, moveTo Prev spacesOnCurrentScreen])
  , ((modMask .|. shiftMask, xK_period),                  mconcat [shiftToNext, moveTo Next spacesOnCurrentScreen])

  -- multiple screens
  , ((modMask, xK_Tab),                                   nextScreen)
  , ((modMask .|. shiftMask, xK_Tab),                     prevScreen)
  -- , ((modMask, xK_Tab),                                   prevScreen)
  -- , ((modMask .|. shiftMask, xK_Tab),                     nextScreen)
  , ((modMask, xK_bracketleft),                           shiftNextScreen)
  , ((modMask, xK_bracketright),                          shiftPrevScreen)
  
  -- , ((modMask .|. shiftMask, xK_q),                       restart "xmonad" True)
  , ((modMask .|. shiftMask, xK_q),                       mconcat [ spawn "killall dzen2"
                                                                  , restart "xmonad" True])
  , ((modMask .|. controlMask .|. shiftMask, xK_q),       io exitSuccess)
  ]

  -- mod-[1..9], Switch to workspace N
  -- mod-shift-[1..9], Move client to workspace N
  -- ++[((m .|. modMask, k), windows $ f i)
  ++[((m .|. modMask, k), windows $ IS.onCurrentScreen f i)
    | (i, k) <- zip (IS.workspaces' conf) [xK_1 .. xK_9]
    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    -- , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

  -- -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
  -- -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
  -- ++[((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
  --   | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
  --   , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myMouseBindings XConfig {XMonad.modMask = modMask} = M.fromList
  [
    -- mod-button1, Set the window to floating mode and move by dragging
    ((modMask, button1), \w -> focus w >> mouseMoveWindow w)

    -- mod-button2, Raise the window to the top of the stack
  , ((modMask, button2), \w -> focus w >> windows W.swapMaster)

    -- mod-button3, Set the window to floating mode and resize by dragging
  , ((modMask, button3), \w -> focus w >> mouseResizeWindow w)

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
  ]

-- myLayout = avoidStruts (
-- myLayout = smartBorders $ avoidStruts (
--   Tall 1 (3/100) (1/2) |||
--   Mirror (Tall 1 (3/100) (1/2)) |||
--   ThreeColMid 1 (3/100) (1/2) |||
--   Full) |||
--   smartBorders ((fullscreenFloat . fullscreenFull) Full)
myLayout = smartBorders $ avoidStruts $ Tall 1 (3/100) (1/2) |||
  Full |||
  smartBorders ((fullscreenFloat . fullscreenFull) Full) |||
  avoidStruts (Mirror (Tall 1 (3/100) (1/2)) |||
  ThreeColMid 1 (3/100) (1/2))
  -- noBorders (fullscreenFull Full)
  
logTitles :: (String -> String) -> Logger
logTitles ppFocus =
  let
    -- windowTitles windowset = sequence (map (fmap showName . getName) (W.index windowset))
    windowTitles windowset = mapM (fmap showName . getName) (W.index windowset)
      where
        showName nw
          | maybe False (== window) fw = ppFocus name
          | otherwise = name
          where        
            fw = W.peek windowset
            window = unName nw
            -- name = shorten 50 (show nw)
            name = shorten (200 `div` length (W.index windowset)) (show nw)
  in
    -- withWindowSet $ fmap (Just . (intercalate " | ")) . windowTitles
    withWindowSet $ fmap (Just . intercalate " | ") . windowTitles

defaults = defaultConfig {
  -- simple stuff
  terminal           = "st -e tmux",
  focusFollowsMouse  = True,
  borderWidth        = 2,
  modMask            = mod4Mask, -- "super"
  workspaces         = map show ([1..9] :: [Int]),
  normalBorderColor  = "#282a2e",
  focusedBorderColor = "#990000",

  -- key bindings
  keys               = myKeys,
  mouseBindings      = myMouseBindings,

  -- hooks, layouts
  layoutHook         = myLayout,
  manageHook         = myManageHook,
  -- startupHook        = return (),
  startupHook        = return (),
  handleEventHook    = mempty <+> docksEventHook <+> E.fullscreenEventHook
  -- handleEventHook    = mconcat [ docksEventHook, handleEventHook defaultConfig, E.fullscreenEventHook ]
  }

main :: IO ()
main = do
  client <- DBus.connectSession

  nScreens <- IS.countScreens
  hostName <- getHostName
  xmproc <- spawnPipe $ xmobarCommandFromHostName hostName
  xmonad $ E.ewmh defaults {
    logHook = dynamicLogWithPP $ xmobarPP {
        ppOutput = hPutStrLn xmproc
        , ppTitle = const ""
        , ppExtras = [logTitles (xmobarColor "#ffd700" "")]
        , ppCurrent = xmobarColor "#f0c674" ""
        }
    , manageHook = manageDocks <+> myManageHook
    , startupHook = setWMName "LG3D"
    -- , workspaces = IS.withScreens nScreens (workspaces defaults)
    , workspaces = IS.withScreens nScreens (map show [1..9])
    }

  -- let pp = defaultPP
  -- xmonad $ E.ewmh defaults { logHook = dbusLog client pp
  --                          , manageHook = manageDocks
  --                          }

xmobarCommandFromHostName :: String -> String
xmobarCommandFromHostName hostName =
  case hostName of
    "pollux" -> "xmobar ~/.xmonad/xmobar_laptop.hs"
    _        -> "xmobar ~/.xmonad/xmobar.hs"
