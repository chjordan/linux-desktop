-- Config { font = "-*-terminus-medium-r-normal-*-12-*-*-*-*-*-*-*"
Config { font = "xft:Terminus (TTF):size=9"
       , additionalFonts = ["-*-siji-*-*-*-*-10-*-*-*-*-*-*-*"]
       , borderColor = "#282a2e"
       , bgColor = "#282a2e"
       , fgColor = "#eeeeee"
       -- , position = TopP 0 180
       , position = Static { xpos = 0 , ypos = 0, width = 1760, height = 16 }
       , commands = [ Run Cpu ["-t","<total>%","-L","25","-H","50","--normal","green","--high","red"] 20
                    , Run Memory ["-t","<usedratio>%","-L","25","-H","50"] 30
                    , Run Swap ["-t","<usedratio>%","-L","1","-H","10"] 30
                    , Run BatteryP ["BAT0"] [] 100
                    , Run DynNetwork ["-L","50","-H","1000","--high","green"] 30
                    , Run Date "%a %b %_d %H:%M" "date" 50
                    , Run StdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{ <fc=#81a2be><fn=1></fn> %cpu%</fc> | <fc=#f0c674><fn=1></fn> %memory% <fn=1></fn> %swap%</fc> | <fc=#b5bd68>%battery%</fc> | <fc=#b294bb>%dynnetwork%</fc> | %date%"
       }
