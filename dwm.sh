#!/bin/bash
# Run dwm in a loop, so restarting dwm is possible.
# Killing this script kills the X session.

# Make sure programs die with this script.
function cleanup {
    killall status
    killall xscreensaver
}
trap cleanup EXIT

# Along with dwm, run these...
wmname LG3D
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
compton &

while true; do
    dwm 2> ~/.dwm.log

    # If dwm crashes, make this not loop crazily.
    sleep 1
done
