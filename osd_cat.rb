#!/usr/bin/env ruby

# osd_cat command
osd = "osd_cat -O1 -p bottom -A center -o50 -c green"

# default device/s
default = "(combined|ladspa.*)"

# increment volume by this percentage
vol_inc = 4

# limit of above 100% volume
vol_limit = 1.5


# killall existing osd_cat instances
system("killall osd_cat")

# pulseaudio controls
case ARGV[0] 
when "pa"
	case ARGV[1]
	when "mute"
		`pacmd dump`.scan(/set-sink-mute #{default} (.*)/).each do |match|
			case match[1]
			when "no"
				# pause mpd
				system("mpc pause")
				`pacmd set-sink-mute #{match[0]} yes`
				`echo "Pulseaudio muted!" | #{osd} -d2`
			when "yes"
				`pacmd set-sink-mute #{match[0]} no`
				`echo "Pulseaudio unmuted!" | #{osd} -d2`
			end
		end
	else
		# adjusting volume
		max_vol = "0x10000".to_i(16)

		# percentage increments
		incr = (max_vol * vol_inc/100).ceil

		# get the current volume
		#current_vol = `pacmd dump`.match(/set-sink-volume combined (.*)/)[1].to_i(16)
		#`pacmd dump`.scan(/set-sink-volume (.*) (0x.*)/).peach do |match|
		match = `pacmd dump`.scan(/set-sink-volume (#{default}) (0x.*)/)
		current_vol = match[0][-1].to_i(16)
		current_vol_pc = (current_vol.to_f/max_vol * 100).round
		case ARGV[1] 
		when "+"
			new_vol = (current_vol + incr)
			new_vol = max_vol if (current_vol_pc < 100 and current_vol_pc + vol_inc > 100)
			new_vol = (max_vol * vol_limit) if (new_vol > vol_limit*max_vol)
		when "-"
			new_vol = (current_vol - incr)
			new_vol = 0 if (current_vol_pc - vol_inc) < 0
			new_vol = max_vol if (current_vol_pc > 100 and current_vol_pc - vol_inc < 100)
		else
			new_vol = ARGV[1].to_f * max_vol / 100
		end
		new_vol_pc = (new_vol.to_f / max_vol * 100).round
		match.each {|device| `pacmd set-sink-volume #{device[0]} $(printf '0x%x' 0x#{new_vol.to_i.to_s(16)})`}
		`echo "Pulseaudio volume: #{new_vol_pc}%" | #{osd} -d1`
	end

# mpd controls
when "mpc"
	case ARGV[1]
	when "vol"
		# for ARGV[2] being either "+" or "-"
		system("mpc volume #{ARGV[2] + vol_inc.to_s}")
		`echo "MPD volume: #{`mpc volume`.match(/volume:\s*(\d*%)/)[1]}" | #{osd} -d1`
	when "seek"
		system("mpc seek #{ARGV[2]}10")
	else
		# {toggle, prev, next} can be fed directly
		system("mpc #{ARGV[1]}")
		`echo $(mpc status) | #{osd} -d3`
	end
end


