#!/usr/bin/env ruby
# Ruby MPD wrapper

# A solution using the ruby gem "librmpd" is provided here, but it is over 10x slower than a regex'ing the output of mpc.
# require 'rubygems'
# require 'librmpd'

# HOST = "localhost"
# PORT = 6600
# mpd = MPD.new HOST, PORT

# mpd.connect
# #mpd.password('mypassword')
# #mpd.play if mpd.stopped?
# if mpd.stopped?
# 	puts "[mpd stopped]"
# 	abort
# end
# song = mpd.current_song

# time_str = mpd.status["time"]
# time = time_str.split(":").map {|t| [ t.to_i/60, sprintf("%02d",t.to_i % 60) ].join(":")}.join("/")

# track = song.track
# artist = song.artist
# title = song.title
# status = mpd.playing? ? true : false
# mpd.disconnect


output = `mpc -f "%artist%---%title%"`.match(/(.*)---(.*)\[(.*)\]\s*#(\d*\/\d*)\s*(\d*:\d*\/\d*:\d*)/m)
abort("no mpd info") if output.to_a.empty?

artist = output[1]
title = output[2]
status = output[3] == "playing" ? true : false
track = output[4]
time = output[5]

# Some hacks to get correct output if id3tags are strange
string = if artist.empty? or title.empty? or artist =~ /artist/i or title =~ /track/i
	(status ? "[Playing" : "[Paused")+" - #{time}] #{song.file}"
else
	(status ? "[Playing" : "[Paused")+" - #{time}] #{track}. #{artist} - #{title}"
end

# string.gsub!(/&/,"&amp;")
puts string
