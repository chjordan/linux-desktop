package main

// TODO
// - is there a file with audio information, like the others in /proc and /sys?
// - make the battery optional, via the error reading the file
// - test lowering the precision of integers and floats
// - better way to strip newlines from strings?
// - better way to shorten the weekday?
// - move regex compiles outside functions

import (
	"fmt"
    "io/ioutil"
    "strconv"
    "strings"
    "time"
	"log"
	"os"
	// "os/exec"
	// "regexp"
)

var	days = []string{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}

func fatalIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// func stripNewLine(slice byte) (output string) {
// 	output = strings.Split(string(slice), "\n")
// 	return
// }

func getCPUSample() (idle, total int64) {
    contents, err := ioutil.ReadFile("/proc/stat")
	fatalIfError(err)
    lines := strings.Split(string(contents), "\n")
	fields := strings.Fields(lines[0])
	for i := 1; i < len(fields); i++ {
		val, err := strconv.ParseInt(fields[i], 10, 64)
		fatalIfError(err)

		total += val
		if i == 4 {
			idle = val
		}
	}
	return
}

func cpuPerc(i0, t0, i1, t1 int64) (string) {
    idleTicks  := i1 - i0
    totalTicks := t1 - t0
	perc := 100 * (totalTicks - idleTicks) / totalTicks
	return fmt.Sprintf("\x0C\uE026%2v%%", perc)
}

func memSwapPerc() (string) {
	var m_total, m_avail, m_perc, s_total, s_free, s_perc int64
	// initialise swap variables, so we can tell if they exist or not
	s_total, s_free = -1, -1
    contents, err := ioutil.ReadFile("/proc/meminfo")
    if err != nil {
		switch err {
		case os.ErrNotExist:
			return ""
		default:
			fatalIfError(err)
		}
	}

    lines := strings.Split(string(contents), "\n")
    for _, line := range(lines) {
        fields := strings.Fields(line)
		switch fields[0] {
		case "MemTotal:":
			m_total, err = strconv.ParseInt(fields[1], 10, 64)
		case "MemAvailable:":
			m_avail, err = strconv.ParseInt(fields[1], 10, 64)
		case "SwapTotal:":
			s_total, err = strconv.ParseInt(fields[1], 10, 64)
		case "SwapFree:":
			s_free, err = strconv.ParseInt(fields[1], 10, 64)
		}
		fatalIfError(err)
		if s_total >= 0 && s_free >= 0 {
			if s_total > 0 && s_free > 0 {
				s_perc = 100 * (s_total - s_free) / s_total
			} else {
				s_perc = -1
			}
			break
		}
	}
	m_perc = 100 * (m_total - m_avail) / m_total

	switch {
	case s_perc > 0:
		return fmt.Sprintf("\x09\uE1AC\uE020%d%% \x06\uE147%d%% \x07", m_perc, s_perc)
	case s_total > 0:
		return fmt.Sprintf("\x09\uE1AC\uE020%d%% \uE147%d%%", m_perc, s_perc)
	default:
		return fmt.Sprintf("\x09\uE1AC\uE020%d%%", m_perc)
	}
}

func battery() (string) {
	contents, err := ioutil.ReadFile("/sys/class/power_supply/BAT0/capacity")
	if os.IsNotExist(err) {
		return ""
	} else {
		fatalIfError(err)
	}
	perc, err := strconv.ParseInt(strings.Split(string(contents), "\n")[0], 10, 64)
	fatalIfError(err)

	current_file, err := ioutil.ReadFile("/sys/class/power_supply/BAT0/current_now")
	if os.IsNotExist(err) {
		return ""
	} else {
		fatalIfError(err)
	}
	current, err := strconv.ParseInt(strings.Split(string(current_file), "\n")[0], 10, 64)
	current /= 100000
	fatalIfError(err)

	status_file, err := ioutil.ReadFile("/sys/class/power_supply/BAT0/status")
	if os.IsNotExist(err) {
		return ""
	} else {
		fatalIfError(err)
	}
	status := string(status_file)

	if status == "Discharging\n" {
		current *= -1
	}

	switch {
	case perc < 20:
		return fmt.Sprintf("\x03\uE1AC\uE01F%d%% (%d)", perc, current)
	case perc < 40:
		return fmt.Sprintf("\x06\uE1AC\uE01F%d%% (%d)", perc, current)
	default:
		return fmt.Sprintf("\x07\uE1AC\uE01F%2d%% (%d)", perc, current)
	}
}

func getNetSample() (rx, tx int64, dev string) {
	contents, err := ioutil.ReadFile("/proc/net/route")
	fatalIfError(err)
	lines := strings.Split(string(contents), "\n")
	// If there's only two lines, there's no network interface
	if len(lines) == 2 {
		return 0, 0, "N/A"
	}
	dev = strings.Fields(lines[1])[0]

	if strings.HasPrefix(dev, "docker") || strings.HasPrefix(dev, "vmnet") {
		return 0, 0, "N/A"
	}

	contents, err = ioutil.ReadFile("/proc/net/dev")
	fatalIfError(err)
    lines = strings.Split(string(contents), "\n")
	for i := 1; i < len(lines); i++ {
		fields := strings.Fields(lines[i])
		if fields[0] == dev + ":" {
			rx, err = strconv.ParseInt(fields[1], 10, 64)
			fatalIfError(err)
			tx, err = strconv.ParseInt(fields[9], 10, 64)
			fatalIfError(err)
			break
		}
	}
	return
}

func netRates(rx_i, tx_i, rx_j, tx_j int64, dev string) (string) {
	// Return nothing, if we're not connected
	if dev == "N/A" {
		return ""
	}

	rxUnit, txUnit := "kiB/s", "kiB/s"
	rx := (float64(rx_j - rx_i)) / (2 * 1024)
	if rx > 1000 {
		rx = rx / 1024
		rxUnit = "MiB/s"
	}
	tx := (float64(tx_j - tx_i)) / (2 * 1024)
	if tx > 1000 {
		tx = tx / 1024
		txUnit = "MiB/s"
	}

	return fmt.Sprintf("\x0B\uE1AC%s \uE061%5.1f%s \uE060%5.1f%s", dev, rx, rxUnit, tx, txUnit)
}

// func volume() (string) {
// 	stdout, err := exec.Command("amixer","get","Master").Output()
// 	fatalIfError(err)

// 	var perc int64
// 	lines := strings.Split(string(stdout), "\n")
// 	for i := 1; i < len(lines); i++ {
// 		fields := strings.Fields(lines[i])
// 		if len(fields) > 1 && fields[0] == "Front" && fields[1] == "Left:" {
// 			vol := fields[4]
// 			perc, err = strconv.ParseInt(vol[1:len(vol)-2], 10, 64)
// 			fatalIfError(err)
// 		}
// 	}

// 	if perc > 100 {
// 		return fmt.Sprintf("\x06\uE015 %d%% \x0B\uE01B", perc)
// 	}
// 	return fmt.Sprintf("\x0B\uE015 %d%% \uE01B", perc)
// }

func timeDate() (string) {
	t := time.Now()
	return fmt.Sprintf("\x04\uE1AC\uE015%s %02d/%02d %02d:%02d", days[int(t.Weekday())], t.Day(), t.Month(), t.Hour(), t.Minute())
}

func main() {
	// Get samples to compare against
    cpu_i0, cpu_t0 := getCPUSample()
	net_rx_i, net_tx_i, _ := getNetSample()

	// Begin looping
	for true {
		time.Sleep(2 * time.Second)

		// cpu
		cpu_i1, cpu_t1 := getCPUSample()
		cpu := cpuPerc(cpu_i0, cpu_t0, cpu_i1, cpu_t1)
		cpu_i0, cpu_t0 = cpu_i1, cpu_t1

		// memory and swap
		mem := memSwapPerc()

		// battery
		bat := battery()

		// IP, Rx and Tx
		net_rx_j, net_tx_j, nic := getNetSample()
		network := netRates(net_rx_i, net_tx_i, net_rx_j, net_tx_j, nic)
		net_rx_i, net_tx_i = net_rx_j, net_tx_j

		// volume
		// vol := volume()
		vol := ""

		// date
		date := timeDate()

		// Put it all together...
		fmt.Printf("%s%s%s%s%s%s\n", cpu, mem, bat, network, vol, date)
	}
}
