#!/bin/bash

set -eu

REPO_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Make the .xmonad directory so that stack stuff doesn't clutter the repo.
mkdir -p ~/.xmonad
for d in $REPO_DIR/dotfiles/*; do
    read -p "Use stow for $d? " RESPONSE
    if [[ $RESPONSE == y || $RESPONSE == Y ]]; then
        cd $d
	    stow -t ~ .
    fi
done
