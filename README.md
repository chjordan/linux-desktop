linux-desktop
=============

I've created this repo to share various scripts and programs I use to manage my linux desktops. Typically these are all running Arch linux (http://www.archlinux.org/). Some of the scripts here are quite old and have not been used in a while, but may come in handy one day. A few of these files normally live in the home directory (~/); I have them symlinked.

Recently, my window manager of choice is dwm (http://dwm.suckless.org/). The files I compile are contained in another repo, along with the patches used. I draw a bit of inspiration from Unia (https://github.com/Unia) - bless you, sir.

My old awesome window manager config is here, although I don't know how reliable it is with modern versions.

Feedback is welcome on improvements.
