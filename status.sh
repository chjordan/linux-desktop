#!/bin/bash
# Just a wrapper to the compiled Go program

REPO_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
$REPO_DIR/status | while read -r; do
    xsetroot -name "$REPLY"
done
