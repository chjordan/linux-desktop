#!/usr/bin/env ruby

# Expects a single ARGV argument, which is something to grep in "lpass ls".

search_term = ARGV.first.downcase
matches = `lpass ls`.scan(/#{search_term}\S*\s+\[id:\s+(\S+)\]/i)
matches.each do |m|
    system("lpass show #{m.first}")
end
