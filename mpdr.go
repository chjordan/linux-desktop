package main

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"unicode"
	"unicode/utf8"
)

func main() {
	stdout, err := exec.Command("mpc","-f","%artist%---%title%").Output()
	if err != nil {
		fmt.Println("mpc failed to connect")
		log.Fatal(err)
	}
	output := string(stdout)

	artist_re  := regexp.MustCompile("^(.*)---")
	song_re    := regexp.MustCompile("---(.*)\n")
	status_re  := regexp.MustCompile(`\n\[([a-z]*)\]`)
	song_no_re := regexp.MustCompile("#([0-9]+/[0-9]+)")
	time_re    := regexp.MustCompile("([0-9]+:[0-9]+/[0-9]+:[0-9]+)")

	if artist_re.MatchString(output) {
		artist  := artist_re.FindStringSubmatch(output)[1]
		song    := song_re.FindStringSubmatch(output)[1]
		status  := status_re.FindStringSubmatch(output)[1]
		song_no := song_no_re.FindStringSubmatch(output)[1]
		time    := time_re.FindStringSubmatch(output)[1]

		// Capitalising the first character of "status"
		r, _ := utf8.DecodeRuneInString(string(status[0]))
		status = string(unicode.ToUpper(r)) + status[1:]

		// If tag information is missing, just display the title
		if artist == "" {
			stdout, err := exec.Command("mpc").Output()
			if err != nil {
				log.Fatal(err)
			}
			output := string(stdout)

			artist_re := regexp.MustCompile("^(.*)\n")
			artist := artist_re.FindStringSubmatch(output)[1]

			fmt.Printf("[%s - %s] %s. %s\n", status, time, song_no, artist)
		} else {
			fmt.Printf("[%s - %s] %s. %s - %s\n", status, time, song_no, artist, song)
		}
	} else {
		fmt.Println("no mpd info")
	}
}
