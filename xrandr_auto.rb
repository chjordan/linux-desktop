#!/usr/bin/env ruby

# This script may be obsoleted by simply running "xrandr --auto"?

# Sets all connected monitors to maximum resolution, and aligns all others to the
# left or right.
# This script assumes the first resolution listed under an interface is the highest.
# The primary screen may be designated with the '-p' options or by hostname in the script.
# If the hostname is not defined, it will also assume that the first connected
# screen in the list is the primary screen.

require 'optparse'

options = {:primary => nil, :xrandr_command => "xrandr", :off => []}
optparse = OptionParser.new do |opts|
	opts.banner = "Sets all connected monitors to maximum resolution, and aligns all others to the left or right.\nThis script assumes the first resolution listed under an interface is the highest.\nThe primary screen may be designated with the '-p' options or by hostname in the script.\nIf the hostname is not defined, it will also assume that the first connected screen in the list is primary."

	opts.on('-h','--help','Display this message') do
		puts opts; exit
	end

	opts.on('-n','--dry','Perform a dry run') do
		options[:dry] = true
	end

	options[:direction] = "right"
	opts.on('-d','--direction left/right','The direction in which screens are added from the primary') do |d|
		if (d.downcase != "right" and d.downcase != "left")
			puts "Direction argument is not valid (left/right)."
			puts "Defaulting to 'right'."
			options[:direction] = "right"
		else
			options[:direction] = d.downcase
		end
	end

	options[:query] = `xrandr -q 2>/dev/null`
	options[:screens] = options[:query].scan(/(\S*) connected.*\n\s*(\d*x\d*)\s*\d*.\d.\+/)
	options[:disscreens] = options[:query].scan(/(\S*) disconnected/)
	options[:allscreens] = options[:query].scan(/(\S*) (dis)?connected/)

	opts.on('-l','--list','List the interfaces available on this machine') do
		puts "Connected screens/interfaces"
		options[:screens].each { |s| puts s[0]; puts s[1] }
		puts "\nDisconnected screens/interfaces"
		options[:disscreens].each { |d| puts d[0] }
		exit
	end

	opts.on('-o','--off screen','Turn this screen off') do |o|
		options[:screens].each do |s|
			break if s[0] == o
			abort("Screen (#{o}) is not connected or interface does not exist.") if s == options[:screens].last
		end
		options[:off][options[:off].length] = o
	end

	opts.on('-p','--primary screen','Set the primary screen') do |p|
		options[:screens].each do |s|
			break if s[0] == p
			abort("Screen (#{p}) is not connected or interface does not exist.") if s == options[:screens].last
		end
		options[:primary] = p
		options[:xrandr_command] << " --output #{p} --primary"
	end
end.parse!


if not options[:primary]
	# determine the hostname, and set the primary screen from that
	# if hostname is not specified, use the first connected screen
	case `echo $HOSTNAME`.strip
	when "bellatrix"
		options[:primary] = options[:query].to_s.scan(/LVDS\S*/)[0]
	when "archimedes"
		options[:primary] = options[:query].to_s.scan(/LVDS\S*/)[0]
	else
		options[:primary] = options[:query].to_s.scan(/(\S*) connected.*\*/m)[0][0]
	end
end
options[:xrandr_command] << " --output #{options[:primary]} --primary"

# turn on all connected screens honouring the direction
previous_screen = options[:primary]
options[:screens].each do |s|
	if s[0] != options[:primary]
		options[:xrandr_command] << " --output #{s[0]} --mode #{s[1]} --#{options[:direction]}-of #{previous_screen}" if not options[:off].include?(s[0])
	else
		options[:xrandr_command] << " --output #{s[0]} --mode #{s[1]}"
	end
end

# turn off all non-connected screens
options[:disscreens].each do |s|
	options[:xrandr_command] << " --output #{s[0]} --off"
end
options[:off].each do |o|
	options[:xrandr_command] << " --output #{o} --off"
end

puts options[:xrandr_command]
`#{options[:xrandr_command]}` if not options[:dry]
