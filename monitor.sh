#!/bin/bash
#change values below to your defaults
x=1920
y=1080
pos=right
if [ -n "$1" ]; then
    x=$1
fi
if [ -n "$2" ]; then
    y=$2
fi
if [ -n "$3" ]; then
    pos=$3
fi
#generate a modeline from x/y
modeline=`cvt $x $y | sed "1d" | sed 's/Modeline //'`
mode=`echo $modeline | sed 's/ .*//'`

#create the mode and ignore xrandr error if the mode is already there
xrandr --newmode $modeline &> /dev/null 2>&1
#add the mode to the other display
xrandr --addmode VIRTUAL1 $mode
#activate external monitor and set options for it
xrandr --output LVDS1 --auto --output VIRTUAL1 --mode $mode --$pos-of LVDS1
#run screenclone in optirun. this way the NVIDIA card will automatically start and shut down. kill with ctrl+c
optirun screenclone -s $DISPLAY -d :8 -x 1
#deactivate monitor after screenclone is killed (kill this script with ctrl+C)
xrandr --output VIRTUAL1 --off
