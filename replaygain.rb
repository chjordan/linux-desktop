#!/usr/bin/env ruby

# Adjusts all music in specified directory that is within some time range.
# This script is dirty and I should feel bad.

home_dir = File.expand_path('~')
music_path = "#{home_dir}/Music/"
#max_time = 60*60*24*7
max_time = 60*60*24*180
current_time = `date +%s`.to_i

Dir.chdir(music_path)
Dir.glob("**/").sort.each do |d|
	Dir.chdir(d)

	trigger = false
	Dir.foreach(Dir.pwd) do |item|
		next if item == '.' or item == '..'
		# if older than the time limit, use this line
		#if (current_time - `date +%s -d "#{File::mtime(item)}"`.to_i) < max_time and File.size?(item) > 1000

		# for songs within the time limit, use this line
		if (current_time - `date +%s -d "#{File::mtime(item)}"`.to_i) > max_time and File.size?(item) > 1000
			trigger = true
			break
		end
	end

	if trigger == false
		puts Dir.pwd
		puts `mp3gain -a *`
	end
	Dir.chdir(music_path)
end
